#!/bin/bash
FILE=$1
if [[ ! -e js/src/i18n/$FILE.json ]]
then
    echo "js/src/i18n/$FILE.json does not exist. Exiting."
    exit 1
else
    LOCALE=$(echo $FILE | sed -e "s@_@-@g")
    json2po -i js/src/i18n/$FILE.json -t js/src/i18n/en.json -o po/$FILE.po
    zanata-cli -q -B push --push-type trans -l $LOCALE
fi
