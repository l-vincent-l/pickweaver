defmodule Pickweaver.Factory do
  use ExMachina.Ecto, repo: Pickweaver.Repo

  def account_factory do
    %Pickweaver.Accounts.Account{
      username: sequence("username"),
      email: sequence(:email, &"email-#{&1}@example.com"),
      password_hash: Comeonin.Argon2.hashpwsalt("mypassword"),
      role: 0,
      slug: sequence("slug"),
      confirmed_at: DateTime.utc_now(),
      confirmation_sent_at: DateTime.utc_now(),
      confirmation_token: :crypto.strong_rand_bytes(30) |> Base.url_encode64
    }
  end

  def story_factory do
    %Pickweaver.Stories.Story{
      title: sequence("mystory"),
      slug: sequence("my-slug"),
      description: "desc",
      elements: [],
      media: build(:file),
      account: build(:account)
    }
  end

  def file_factory do
    %Pickweaver.Files.File{
      uuid: sequence("uuid"),
      path: sequence("path"),
      name: sequence("name"),
      account: build(:account)
    }
  end

  def import_factory do
    %Pickweaver.Import{
      url: "https://twitter.com/nitot/status/957963447513296897",
      tentatives: 0,
      account: build(:account),
    }
  end
end
