defmodule PickweaverWeb.AccountControllerTest do
  use PickweaverWeb.ConnCase

  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.Account
  import Pickweaver.Factory

  @create_attrs %{email: "some@email.com", password: "some password", role: 42, username: "some username"}
  @update_attrs %{email: "some@email.updated", password: "some updated password", role: 43, username: "some updated username"}
  @invalid_attrs %{email: nil, password: nil, role: nil, username: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create account" do
    test "renders account when data is valid", %{conn: conn} do
      email = @create_attrs.email
      conn = post conn, account_path(conn, :create), @create_attrs
      assert %{"email" => email} = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, account_path(conn, :create), @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "resent account confirmation" do
    setup [:create_unconfirmed_account]

    test "resend confirmation account with valid email", %{conn: conn, account: %Account{} = account} do
      {:ok, account} = Accounts.update_account(account, %{"confirmation_sent_at" => Timex.shift(account.confirmation_sent_at, hours: -2)})
      account_email = account.email
      conn = post conn, account_path(conn, :resend_confirmation), %{"email" => account_email}
      assert %{"email" => account_email} = json_response(conn, 200)
    end

    test "resend confirmation account with invalid email", %{conn: conn, account: %Account{} = account} do
      account_email = "toto@tata"
      conn = post conn, account_path(conn, :resend_confirmation), %{"email" => account_email}
      assert %{"error" => "Unable to find an account with this email"} = json_response(conn, 404)
    end
  end

  describe "reset password" do
    setup [:create_account]

    use Bamboo.Test

    test "send reset password email", %{conn: conn, account: %Account{} = account} do
      account_email = account.email
      conn = post conn, account_path(conn, :send_reset_password), %{"email" => account_email}
      assert %{"email" => account_email} = json_response(conn, 200)

      {:ok, account_updated} = Accounts.find_by_email(account_email)

      assert_delivered_email Pickweaver.Email.Account.reset_password_email(account_updated)

      token = account_updated.reset_password_token

      conn = post conn, account_path(conn, :reset_password), %{"token" => token, "password" => "mypassword"}
      assert %{"account" => _account, "token" => _token} = json_response(conn, 200)

      conn = post conn, session_path(conn, :sign_in), %{"username" => account.username, "password" => "mypassword"}
      assert %{"token" => token, "user" => user} = json_response(conn, 200)
    end

    test "send reset password to wrong address", %{conn: conn} do
      conn = post conn, account_path(conn, :send_reset_password), %{"email" => "toto"}
      assert %{"errors" => "Unable to find an account with this email"} = json_response(conn, 404)
    end

    test "reset from bad token", %{conn: conn} do
      conn = post conn, account_path(conn, :reset_password), %{"token" => "toto", "password" => "mypassword"}
      assert %{"errors" => %{"token" => ["Wrong token for password reset"]}} = json_response(conn, 404)
    end

    test "reset with new password not long enough", %{conn: conn, account: %Account{} = account} do
      Pickweaver.Accounts.Service.ResetPassword.send_password_reset_email(account)
      {:ok, account_updated} = Accounts.find_by_email(account.email)

      conn = post conn, account_path(conn, :reset_password), %{"token" => account_updated.reset_password_token, "password" => "a"}
      assert %{"errors" => %{"password" => ["registration.error.password_too_short"]}} = json_response(conn, 422)
    end
  end

  describe "show account" do
    setup [:create_account]

    test "show account from slug name", %{conn: conn, account: %Account{} = account} do
      conn = auth_conn(conn, account)
      account2 = insert(:account)
      conn = get conn, account_path(conn, :show, account2.slug)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => account2.slug,
        "role" => account2.role,
        "username" => account2.username,
        "stats" => %{"stories" => 0}
      }
    end

    test "show account from invalid username", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = get conn, account_path(conn, :show, "hey")
      assert response(conn, 404)
    end

    test "show account currently logged in", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = get conn, account_path(conn, :show_current_account)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => slug
             }
    end

    test "show account currently logged in when user is unlogged", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = get conn, account_path(conn, :show_current_account)
      assert response(conn, 401)
    end
  end

  describe "update account" do
    setup [:create_account]

    @tag :pending
    test "renders account when data is valid", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = put conn, account_path(conn, :update), account: @update_attrs
      assert %{"slug" => slug} = json_response(conn, 200)["account"]

      conn = get conn, account_path(conn, :show_current_account)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => slug,
        "role" => 43,
        "username" => "some updated username"}
    end

    test "renders errors when data is invalid", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = put conn, account_path(conn, :update), account: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete account" do
    setup [:create_account]

    test "deletes own account", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = delete conn, account_path(conn, :delete)
      assert response(conn, 204)

      conn = get conn, account_path(conn, :show_current_account)
      assert response(conn, 401)
    end

    test "delete account without being logged-in", %{conn: conn, account: %Account{} = account} do
      conn = delete conn, account_path(conn, :delete)
      assert response(conn, 401)
    end
  end

  defp create_account(_) do
    account = insert(:account)
    {:ok, account: account}
  end

  defp create_unconfirmed_account(_) do
    account = insert(:account, %{confirmed_at: nil})
    {:ok, account: account}
  end

  defp auth_conn(conn, %Pickweaver.Accounts.Account{} = user) do
    {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(user)
    conn
    |> put_req_header("authorization", "Bearer #{token}")
    |> put_req_header("accept", "application/json")
  end
end
