defmodule PickweaverWeb.SessionControllerTest do
  use PickweaverWeb.ConnCase
  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.Account

  import Pickweaver.Factory

  @create_attrs %{email: "some@email.com", password: "some password", role: 42, username: "myusername"}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "login confirmed account" do
    setup [:create_account]

    test "login to account with right credentials", %{conn: conn, account: %Account{username: username} = account} do
      creds = %{"username" => username, "password" => @create_attrs.password}
      conn = post conn, session_path(conn, :sign_in), creds
      assert %{"token" => token, "user" => user} = json_response(conn, 200)
      assert %{"username" => username} = user
    end

    test "login to account with incorrect password", %{conn: conn, account: %Account{username: username} = account} do
      creds = %{"username" => username, "password" => "toto"}
      conn = post conn, session_path(conn, :sign_in), creds
      assert %{"error_msg" => "Bad login","display_error" => "session.error.bad_login"} = json_response(conn, 401)
    end

    test "login to account with inexistant username", %{conn: conn, account: %Account{username: username} = account} do
      creds = %{"username" => "noone", "password" => "toto"}
      conn = post conn, session_path(conn, :sign_in), creds
      assert %{"error_msg" => "No such user","display_error" => "session.error.bad_login"} = json_response(conn, 401)
    end
  end

  describe "login unconfirmed account" do
    setup [:create_unconfirmed_account]

    test "test to login with unconfirmed account", %{conn: conn, account: %Account{username: username} = account} do
      creds = %{"username" => username, "password" => @create_attrs.password}
      conn = post conn, session_path(conn, :sign_in), creds
      assert %{"error_msg" => "User is not activated","display_error" => "session.error.not_activated"} = json_response(conn, 401)
    end
  end

  defp create_account(_) do
    account = insert(:account, %{username: @create_attrs.username, password_hash: Comeonin.Argon2.hashpwsalt(@create_attrs.password)})
    {:ok, account: account}
  end

  defp create_unconfirmed_account(_) do
    account = insert(:account, %{confirmed_at: nil})
    {:ok, account: account}
  end
end
