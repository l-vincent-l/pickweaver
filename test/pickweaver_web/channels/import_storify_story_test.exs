defmodule PickweaverWeb.ImportStorifyStoryChannelTest do
  @moduledoc """
  Test the `ImportTwitterMomentChannel` module
  """
  alias PickweaverWeb.UserSocket
  use PickweaverWeb.ChannelCase
  import Pickweaver.Factory
  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.Account

  setup do
    user = insert(:account)
    {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(user)

    {:ok, socket} = connect(UserSocket, %{"token" => token})

    {:ok, socket: socket, user: user}
  end

  test "connects to storify importer websocket with user without twitter auth", %{socket: socket, user: user} do
    assert {:error, _} = subscribe_and_join(socket, "import:storify:" <> user.username)
  end

  test "connects to storify importer websocket with user with expired twitter auth", %{socket: socket, user: user} do
    add_twitter_token_for_user(user)
    assert {:error, _} = subscribe_and_join(socket, "import:storify:" <> user.username)
  end

  @tag :pending
  test "sends the list of stories for some storify username", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:storify:" <> user.username)
    push socket, "profile", %{"body" => %{"account_name" => "TeamUSA"}}
    assert_broadcast "fetched_page", %{page: 1}, 2000
    assert_broadcast "fetched_page", %{page: 2}, 4000
    assert_broadcast "fetched_page", %{page: 3}, 6000
    assert_broadcast "return_profile", %{stories: [%{url: "https://storify.com/TeamUSA/winterfest-5a9579047d879f62728daa70"}]}, 10000
  end

  @tag :pending
  test "handle import stories", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:storify:" <> user.username)
    push socket, "import", %{"body" => %{"stories" => [%{"url" => "https://storify.com/nicosomb/conversation-with-pi0u-and-papygeek"}]}}
    assert_broadcast "saved_stories", %{}
    assert_broadcast "processed_story", %{story: %{slug: "conversation-with-atpi0u-and-atpapygeek", url: "https://storify.com/nicosomb/conversation-with-pi0u-and-papygeek", status: :ok}}, 2000
  end

  defp add_twitter_token_for_user(%Account{} = user) do
    Accounts.create_token(%{"service" => "twitter", "data" => %{"access_token" => %{"oauth_token" => "foo", "oauth_token_secret" => "bar"}}, "account_id" => user.id })
  end
end
