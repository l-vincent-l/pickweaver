defmodule PickweaverWeb.ImportTwitterMomentChannelTest do
  @moduledoc """
  Test the `ImportTwitterMomentChannel` module
  """
  alias PickweaverWeb.UserSocket
  use PickweaverWeb.ChannelCase
  import Pickweaver.Factory

  setup do
    user = insert(:account)
    {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(user)

    {:ok, socket} = connect(UserSocket, %{"token" => token})

    {:ok, socket: socket, user: user}
  end

  test "connects to twitter moment importer websocket", %{socket: socket, user: user} do
    assert {:ok, _, _} = subscribe_and_join(socket, "import:twitter_moment:" <> user.username)
  end

  @tag :pending
  test "provides some twitter moment url", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:twitter_moment:" <> user.username)
    push socket, "import", %{"body" => %{"moment" => "https://twitter.com/i/moments/650667182356082688"}}
    assert_broadcast "saved_import", %{import: _}, 1000
    assert_broadcast "start_processing", %{story: %{url: "https://twitter.com/i/moments/650667182356082688"}}, 1000
    assert_broadcast "processed_story", %{story: %{slug: "the-obamas-wedding-anniversary", status: :ok, url: "https://twitter.com/i/moments/650667182356082688"}}, 3000
  end

  test "provides an url that isn't a moment", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:twitter_moment:" <> user.username)
    push socket, "import", %{"body" => %{"moment" => "https://framablog.org/2017/10/09/contributopia-degoogliser-ne-suffit-pas/"}}
    assert_broadcast "failed_story", %{story: %{url: "https://framablog.org/2017/10/09/contributopia-degoogliser-ne-suffit-pas/", status: :failure}}
  end

  test "provices a text that isn't an URL", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:twitter_moment:" <> user.username)
    push socket, "import", %{"body" => %{"moment" => "foo bar"}}
    assert_broadcast "failed_story", %{story: %{url: "foo bar", status: :failure}}
  end
end
