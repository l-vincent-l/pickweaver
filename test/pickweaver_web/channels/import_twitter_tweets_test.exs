defmodule PickweaverWeb.ImportTwitterTweetsChannelTest do
  @moduledoc """
  Test the `ImportTwitterTweetsChannel` module
  """
  alias PickweaverWeb.UserSocket
  use PickweaverWeb.ChannelCase
  import Pickweaver.Factory

  setup do
    user = insert(:account)
    {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(user)

    {:ok, socket} = connect(UserSocket, %{"token" => token})

    {:ok, socket: socket, user: user}
  end

  test "connects to twitter tweet importer websocket", %{socket: socket, user: user} do
    assert {:ok, _, _} = subscribe_and_join(socket, "import:twitter_tweets:" <> user.username)
  end

  @tag :pending
  test "provides some twitter url", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:twitter_tweets:" <> user.username)
    push socket, "import", %{"body" => %{"tweet" => "https://twitter.com/nitot/status/957963447513296897"}}
    assert_broadcast "saved_import", %{import: _}, 1000
    assert_broadcast "processed_story", %{story: %{slug: "tweets-from-nitot", status: :ok, url: "https://twitter.com/nitot/status/957963447513296897"}}, 3000
  end

  test "provides an url that isn't a tweet", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:twitter_tweets:" <> user.username)
    push socket, "import", %{"body" => %{"tweet" => "https://framablog.org/2017/10/09/contributopia-degoogliser-ne-suffit-pas/"}}
    assert_broadcast "failed_story", %{story: %{url: "https://framablog.org/2017/10/09/contributopia-degoogliser-ne-suffit-pas/", status: :failure, reason: :not_tweet}}
  end

  test "provices a text that isn't an URL", %{socket: socket, user: user} do
    {:ok, _, socket} = subscribe_and_join(socket, "import:twitter_tweets:" <> user.username)
    push socket, "import", %{"body" => %{"tweet" => "foo bar"}}
    assert_broadcast "failed_story", %{story: %{url: "foo bar", status: :failure, reason: :not_tweet}}
  end
end
