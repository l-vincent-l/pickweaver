defmodule Pickweaver.Services.TwitterMomentTest do
  @moduledoc """
  Test the `Pickweaver.Services.Import.TwitterMoment` module
  """

  use Pickweaver.DataCase
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

  alias Pickweaver.Services.Import.TwitterMoment
  import Pickweaver.Factory

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes")
    :ok
  end

  describe "importing a moment" do
    test "import a moment from html" do
      use_cassette "tweets_from_moment_html" do
        account = insert(:account)
        # TODO : Move this fixture to Cassette
        moment_html = File.read!("./test/fixtures/twitter/moment.html")
        assert {:ok, %{
          "title" => "The Obamas' wedding anniversary",
          "description" => "Yesterday, President and Mrs. Obama celebrated 23 years of marriage."
               }} = TwitterMoment.import_tweets_from_moment_html(moment_html, account)
      end
    end
  end
end
