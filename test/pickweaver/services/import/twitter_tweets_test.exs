defmodule Pickweaver.Services.TwitterTweetsTest do
  @moduledoc """
  Test the `Pickweaver.Services.Import.TwitterTweets` module
  """

  use Pickweaver.DataCase
  alias Pickweaver.Services.Import.TwitterTweets
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes")
    :inets.start
    :ok
  end

  describe "importing a tweet" do
    test "import a tweet from a Twitter URL" do
      use_cassette "twitt-nitot" do
        assert {:ok, %{"title" => "Tweets from nitot", "description" => "Dear Twitter, I was wondering how many https://t.co/jhfaosq9L1 users there are. Any idea?"}} = TwitterTweets.import_from_twitter_url("https://twitter.com/nitot/status/957963447513296897")
      end
    end

    test "import a tweet from a wrong URL" do
      tweets = use_cassette "tweets-from-wrong-url" do
        TwitterTweets.import_from_twitter_url("toto")
      end
      assert {:error, :not_tweet} == tweets
    end
  end
end
