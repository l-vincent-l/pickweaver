defmodule Pickweaver.ParserTest do
  use Pickweaver.DataCase

  alias Pickweaver.Services.Parser
  alias Pickweaver.Services.Parser.{Tweet, Opengraph, Youtube, Facebook}
  import Pickweaver.Factory

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes")
    :ok
  end

  describe "parser (twitter)" do
    use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

    test "process_multiple_urls/1 with valid urls" do
      use_cassette "nitots-arretssurimage" do
        account = insert(:account)
        tweets = Parser.process_multiple_urls(
          [
            "https://twitter.com/nitot/status/957963447513296897",
            "https://twitter.com/arretsurimages/status/951521164056256513"
          ],
          account
        )

        assert [
                 %Tweet{
                   text: "Dear Twitter, I was wondering how many http://Storify.com users there are. Any idea?",
                   user: %{
                     screen_name: "nitot"
                   }
                 },
                 %Tweet{
                   text: "Eh bien ne le répétez pas, ne le re-tweetez pas, n'en parlez pas à vos amis, ne venez pas trop nombreux, mais il semble bien que cette fois, notre serveur tient le coup... Chut ! https://beta.arretsurimages.net/ #angoisse #montéeencharge",
                   user: %{
                     screen_name: "arretsurimages"
                   }
                 }
               ] = tweets
      end
    end

    test "process_single_url/1 with valid url from Twitter" do
      data = %{
        url: "https://twitter.com/TheRedFag/status/978674197940580352",
        type: "Twitter",
        data: %Tweet{
          text: "L'emploi des mots à caractère homophobe, même sans mauvaise intention et quel que soit le contexte, contribue à banaliser l'homophobie. "
        }
      }
      res = use_cassette "single-url-Twitter" do
        account = insert(:account)
        Parser.process_single_url(data.url, account)
      end
    end
  end
end
