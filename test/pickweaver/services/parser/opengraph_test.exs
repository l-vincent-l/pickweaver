defmodule Pickweaver.Services.OpengraphTest do
  @moduledoc """
  Test the `Pickweaver.Services.Parser.Opengraph` module
  """

  use Pickweaver.DataCase
  alias Pickweaver.Services.Parser.Opengraph

  describe "processing opengraph urls" do

    test "processing an opengraph URL" do
      assert {:ok, %Opengraph{
               title: "Réforme de la SNCF : les syndicats tentent d’organiser la riposte",
             }} = Opengraph.parse_url("http://www.lemonde.fr/economie/article/2018/02/27/les-syndicats-de-la-sncf-et-du-secteur-public-preparent-leur-riposte_5263052_3234.html")
    end

    test "transform a list of URLs info opengraph elements" do
      assert [
               %Opengraph{
        title: "Réforme de la SNCF : les syndicats tentent d’organiser la riposte",
             }, %Opengraph{
        title: "Le bouquet britannique Sky au cœur d’une bataille entre barons des médias",
               }
             ] = Opengraph.parse_urls(["http://www.lemonde.fr/economie/article/2018/02/27/les-syndicats-de-la-sncf-et-du-secteur-public-preparent-leur-riposte_5263052_3234.html", "http://www.lemonde.fr/actualite-medias/article/2018/02/27/television-payante-l-americain-comcast-veut-racheter-le-britannique-sky_5263026_3236.html"])
    end
  end
end
