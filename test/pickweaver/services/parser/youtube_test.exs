defmodule Pickweaver.Services.YoutubeTest do
  @moduledoc """
  Test the `Pickweaver.Services.Parser.Youtube` module
  """

  use Pickweaver.DataCase
  alias Pickweaver.Services.Parser.Youtube
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  setup_all do
    ExVCR.Config.filter_url_params(true)
    ExVCR.Config.filter_sensitive_data("oauth_signature=[^\"]+", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("guest_id=.+;", "<REMOVED>")
    ExVCR.Config.filter_sensitive_data("access_token\":\".+?\"", "access_token\":\"<REMOVED>\"")
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes")
    :ok
  end

  describe "processing youtube videos" do

    test "processing a youtube video" do
      tube = use_cassette "youtube-gardiens" do
        Youtube.parse_youtube_url("https://www.youtube.com/watch?v=t5tBsVX5g0g")
      end

      assert {:ok, %Youtube{
        id: "t5tBsVX5g0g",
        title: "Les gardiens du nouveau monde, doc 55', VF",
      }} = tube
    end

    test "transform a list of URLs info youtube elements" do
      tubes = use_cassette "youtube-elements" do
        Youtube.parse_youtube_urls(["https://www.youtube.com/watch?v=t5tBsVX5g0g", "https://www.youtube.com/watch?v=19wToRIiYWI"])
      end
      assert [%Youtube{}, %Youtube{}] = tubes
    end
  end
end
