defmodule Pickweaver.StoriesTest do
  use Pickweaver.DataCase

  import Pickweaver.Factory

  alias Pickweaver.Stories
  alias Pickweaver.Stories.{Story, Element}

  describe "stories" do

    @valid_attrs %{title: "some title", description: "My short story", elements: []}
    @update_attrs %{title: "some updated title", description: "My short story updated", elements: []}
    @invalid_attrs %{title: nil, description: nil, elements: nil}

    def account_fixture() do
      insert(:account)
    end

    def story_fixture() do
      insert(:story)
    end

    test "list_stories/0 returns all stories" do
      assert Stories.list_stories() == %Scrivener.Page{entries: [], total_entries: 0, page_number: 1, page_size: 10, total_pages: 1}
      story = story_fixture()
      stories = Stories.list_stories()
      assert stories = %Scrivener.Page{entries: [story], total_entries: 1, page_number: 1, page_size: 10, total_pages: 1}
    end

    test "list_stories_for_account/2 returns the stories for an account" do
      story = story_fixture()
      _ = story_fixture()
      stories = Stories.list_stories_for_account(story.account)
      assert stories = %Scrivener.Page{entries: [story], total_entries: 1, page_number: 1, page_size: 10, total_pages: 1}
    end

    test "list_stories_for_account/2 returns no stories for an account without" do
      _ = story_fixture()
      _ = story_fixture()
      account = account_fixture()
      assert Stories.list_stories_for_account(account) == %Scrivener.Page{entries: [], total_entries: 0, page_number: 1, page_size: 10, total_pages: 1}
    end

    test "list_stories_for_account_slug/2 returns the stories for an account slug" do
      story = story_fixture()
      _ = story_fixture()
      stories = Stories.list_stories_for_account_slug(story.account.slug)
      assert stories = %Scrivener.Page{entries: [story], total_entries: 1, page_number: 1, page_size: 10, total_pages: 1}
    end

    test "search_stories/2 returns the stories for a search term" do
      account = account_fixture()
      valid_attrs = Map.put(@valid_attrs, :account_id, account.id)
      valid_attrs = Map.put(valid_attrs, :title, "mon histoire")
      {:ok, %Story{} = story} = Stories.create_story(valid_attrs)
      stories = Stories.search_stories(%{"search" => "histoire"})
      assert stories = %Scrivener.Page{entries: [story], total_entries: 1}
    end

    test "get_story!/1 returns the story with given id" do
      story = story_fixture()
      assert Stories.get_story!(story.id).title == story.title
    end

    test "create_story/1 with valid data creates a story" do
      account = account_fixture()
      valid_attrs = Map.put(@valid_attrs, :account_id, account.id)
      assert {:ok, %Story{} = story} = Stories.create_story(valid_attrs)
      assert story.title == "some title"
    end

    test "create_story/1 with invalid data returns error changeset" do
      account = account_fixture()
      invalid_attrs = Map.put(@invalid_attrs, :account_id, account.id)
      assert {:error, %Ecto.Changeset{}} = Stories.create_story(invalid_attrs)
    end

    test "update_story/2 with valid data updates the story" do
      story = story_fixture()
      account = account_fixture()
      update_attrs = Map.put(@update_attrs, :account_id, account.id)
      assert {:ok, story} = Stories.update_story(story, update_attrs)
      assert %Story{} = story
      assert story.title == "some updated title"
    end

    test "update_story/2 with invalid data returns error changeset" do
      story = story_fixture()
      account = account_fixture()
      invalid_attrs = Map.put(@invalid_attrs, :account_id, account.id)
      assert {:error, %Ecto.Changeset{}} = Stories.update_story(story, invalid_attrs)
      assert story = Stories.get_story!(story.id)
    end

    test "delete_story/1 deletes the story" do
      story = story_fixture()
      assert {:ok, %Story{}} = Stories.delete_story(story)
      assert_raise Ecto.NoResultsError, fn -> Stories.get_story!(story.id) end
    end

    test "change_story/1 returns a story changeset" do
      story = story_fixture()
      assert %Ecto.Changeset{} = Stories.change_story(story)
    end
  end

  describe "elements" do

    alias Pickweaver.Services.Parser.Tweet
    alias Pickweaver.Services.Parser.Text

    @element %Tweet{
      id: "957963447513296897",
      user: %{},
      text: "Dear Twitter, I was wondering how many http://Storify.com users there are. Any idea?",
      created_at: "now",
      images: [],
      force_url: "https://twitter.com/nitot/status/957963447513296897",
      hashtags: [],
      mentions: [],
    }
    @text %Text{
      text: "My text element"
    }

    test "create test element and get it" do
      assert [%Element{} = element] = Stories.create_elements([@text])
      assert element.data["text"] == "My text element"

      assert element == Stories.get_element!(element.id)
    end

    test "process_elements creates elements" do
      assert [%Element{} = element] = Stories.create_elements([@element])
      assert element.data["text"] == "Dear Twitter, I was wondering how many http://Storify.com users there are. Any idea?"
      assert element.url == @element.force_url
    end
  end
end
