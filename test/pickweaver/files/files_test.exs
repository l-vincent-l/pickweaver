defmodule Pickweaver.FilesTest do
  use Pickweaver.DataCase

  alias Pickweaver.Files
  import Pickweaver.Factory

  describe "files" do
    alias Pickweaver.Files.File

    @valid_attrs %{path: "some path", uuid: "some uuid"}
    @update_attrs %{path: "some updated path", uuid: "some updated uuid"}
    @invalid_attrs %{path: nil}

    def account_fixture do
      insert(:account)
    end

    def file_fixture(attrs \\ %{}) do
      account = account_fixture()
      valid_attrs = Map.put(@valid_attrs, :account_id, account.id)
      {:ok, file} =
        attrs
        |> Enum.into(valid_attrs)
        |> Files.create_file()

      file
    end

    test "list_files/0 returns all files" do
      file = file_fixture()
      assert Files.list_files() == [file]
    end

    test "get_file!/1 returns the file with given id" do
      file = file_fixture()
      assert Files.get_file!(file.id) == file
    end

    test "create_file/1 with valid data creates a file" do
      account = account_fixture()
      valid_attrs = Map.put(@valid_attrs, :account_id, account.id)
      assert {:ok, %File{path: "some path"}} = Files.create_file(valid_attrs)
    end

    test "create_file/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Files.create_file(@invalid_attrs)
    end

    test "update_file/2 with valid data updates the file" do
      file = file_fixture()
      assert {:ok, %File{path: "some updated path"}} = Files.update_file(file, @update_attrs)
    end

    test "update_file/2 with invalid data returns error changeset" do
      file = file_fixture()
      assert {:error, %Ecto.Changeset{}} = Files.update_file(file, @invalid_attrs)
      assert file == Files.get_file!(file.id)
    end

    test "get_file_by_uuid/1 with valid UUID" do
      file = file_fixture()
      assert file == Files.get_file_by_uuid!(file.uuid)
    end

    test "get_file_by_uuid/1 with invalid UUID" do
      assert_raise Ecto.NoResultsError, fn -> Files.get_file_by_uuid!("my uuid") end
    end

    test "delete_file/1 deletes the file" do
      file = file_fixture()
      assert {:ok, %File{}} = Files.delete_file(file)
      assert_raise Ecto.NoResultsError, fn -> Files.get_file!(file.id) end
    end

    test "change_file/1 returns a file changeset" do
      file = file_fixture()
      assert %Ecto.Changeset{} = Files.change_file(file)
    end
  end
end
