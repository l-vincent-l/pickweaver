defmodule Pickweaver.Repo.Migrations.AddStatusToImports do
  use Ecto.Migration

  def up do
    alter table("imports") do
      add :status, :integer, [null: false, default: 0]
    end
  end

  def down do
    alter table("imports") do
      remove :status
    end
  end
end
