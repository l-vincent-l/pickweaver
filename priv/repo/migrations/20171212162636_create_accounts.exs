defmodule Pickweaver.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :username, :string
      add :email, :string
      add :password_hash, :string
      add :role, :integer

      timestamps()
    end

    create unique_index(:accounts, [:email])

  end
end
