defmodule Pickweaver.Repo.Migrations.IncreaseTitleAndSlugSize do
  use Ecto.Migration

  def change do
    alter table(:stories) do
      modify :title, :string, [size: 300]
      modify :slug, :string, [size: 350]
    end

  end
end
