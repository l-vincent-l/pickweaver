defmodule Pickweaver.Repo.Migrations.SetElementsInsideStoriesAsArray do
  use Ecto.Migration

  def change do
    alter table(:stories) do
      remove :elements
      add :elements, {:array, :uuid}
    end
  end
end
