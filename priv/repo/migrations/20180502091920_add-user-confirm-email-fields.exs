defmodule :"Elixir.Pickweaver.Repo.Migrations.Add-user-confirm-email-fields" do
  use Ecto.Migration

  def up do
    alter table(:accounts) do
      add :confirmed_at, :utc_datetime
      add :confirmation_sent_at, :utc_datetime
      add :confirmation_token, :string
    end
    create unique_index("accounts", [:confirmation_token], name: "index_unique_accounts_confirmation_token")
  end

  def down do
    alter table(:accounts) do
      remove :confirmed_at
      remove :confirmation_sent_at
      remove :confirmation_token
    end
  end
end
