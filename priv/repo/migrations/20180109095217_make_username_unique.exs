defmodule Pickweaver.Repo.Migrations.MakeUsernameUnique do
  use Ecto.Migration

  def change do
    create unique_index(:accounts, [:username])
  end
end
