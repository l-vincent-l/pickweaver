const Dotenv = require('dotenv-webpack');

module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    plugins: [
      new Dotenv(),
    ],
  },
  pwa: {
    name: 'Pickweaver',
    themeColor: '#17a2b8',
    msTileColor: '#17a2b8',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'GenerateSW',
  }
};
