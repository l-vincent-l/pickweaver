import Vue from 'vue';
import Vuex from 'vuex';
import vuexI18n from 'vuex-i18n';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/scss/bootstrap.scss';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import moment from 'moment';
import VueMoment from 'vue-moment';
import VueClazyLoad from 'vue-clazy-load';
import Snotify, { SnotifyPosition } from 'vue-snotify';
import 'vue-snotify/styles/material.css';
import App from '@/App';
import router from '@/router';
import storeData from '@/store';
import '@/filters';
import { detectLang } from '@/i18n';
import fallbackTranslation from '@/i18n/en_US.json';
import './registerServiceWorker';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.use(VueClazyLoad);

const options = {
  toast: {
    position: SnotifyPosition.rightTop,
  },
};

Vue.use(Snotify, options);

const store = new Vuex.Store(storeData);
Vue.use(vuexI18n.plugin, store);

Vue.i18n.add('en_US', fallbackTranslation);
Vue.i18n.fallback('en_US');
Vue.i18n.set('en_US');

const loadedLanguages = ['en']; // our default language that is prelaoded

function setI18nLanguage(lang) {
  Vue.i18n.set(lang);
  // TODO : set the 'Accept-Language' header in storyFetch to match lang;
  document.querySelector('html').setAttribute('lang', lang.replace(/_/, '-'));
  return lang;
}

function loadLanguageAsync(lang) {
  if (Vue.i18n.locale() !== lang) {
    if (!loadedLanguages.includes(lang)) {
      import(/* webpackChunkName: "lang-[request]" */ `@/i18n/${lang}.json`).then((msgs) => {
        Vue.i18n.add(lang, msgs);
        loadedLanguages.push(lang);
        return setI18nLanguage(lang);
      });
      import(/* webpackChunkName: "lang-moment-[request]" */`moment/locale/${lang.split('_')[0]}`);
    }
    return Promise.resolve(setI18nLanguage(lang));
  }
  return Promise.resolve(lang);
}

Vue.use(VueMoment, {
    moment,
});

loadLanguageAsync(detectLang());

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
