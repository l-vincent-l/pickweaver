import { API_ORIGIN, API_PATH } from './_entrypoint';

export default function storyFetch(url, store, optionsarg = {}) {
  const options = optionsarg;
  if (typeof options.headers === 'undefined') {
    options.headers = new Headers();
  }
  /* if (options.headers.get('Accept') === null) {
    options.headers.set('Accept', 'application/json');
  } */

  if (options.body !== 'undefined' && !(options.body instanceof FormData) && options.headers.get('Content-Type') === null) {
    options.headers.set('Content-Type', 'application/json');
  }

  if (store.state.isLogged) {
    options.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
  }

  const link = url.includes(API_PATH) ? API_ORIGIN + url : API_ORIGIN + API_PATH + url;

  return fetch(link, options).then((response) => {
    if (response.status === 200 || response.status === 201) {
      return response.json();
    }
    if (response.ok) {
      return response;
    }
    if (response.status === 422 || response.status === 404) {
      throw response.json();
    }
    throw response;
  });
}
