/* eslint-disable */

export const API_HOST = process.env.API_HOST;
export const API_ORIGIN = process.env.API_ORIGIN;
export const API_WEBSOCKET = process.env.API_WEBSOCKET;
export const API_PATH = process.env.API_PATH;

/* eslint-enable */
