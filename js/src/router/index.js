import Vue from 'vue';
import Router from 'vue-router';
import auth from '@/auth/index';
import PageNotFound from '@/components/PageNotFound';
import Home from '@/components/Home';
import MyStories from '@/components/MyStories';
import Stories from '@/components/StoryList';
import Search from '@/components/Search';
import Story from '@/components/story/Story';
import NewStory from '@/components/story/NewStory';
import Login from '@/components/account/Login';
import Register from '@/components/account/Register';
import Validate from '@/components/account/Validate';
import ResendConfirmation from '@/components/account/ResendConfirmation';
import SendPasswordReset from '@/components/account/SendPasswordReset';
import PasswordReset from '@/components/account/PasswordReset';
import Profile from '@/components/account/Profile';
import EditProfile from '@/components/account/EditProfile';
import Connect from '@/components/connect/Connect';
import ConnectCallback from '@/components/connect/ConnectCallback';

import importRoutes from '@/router/import';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/stories',
      name: 'Stories',
      component: Stories,
    },
    {
      path: '/my-stories/:page?',
      name: 'MyStories',
      component: MyStories,
      props: true,
    },
    {
      path: '/stories/:search_term',
      name: 'Search',
      component: Search,
      props: true,
    },
    {
      path: '/story/:username/:slug/:preview(preview)?',
      name: 'Story',
      component: Story,
      props: true,
    },
    {
      path: '/story/:username/:slug/edit',
      name: 'EditStory',
      component: NewStory,
      props: true,
      meta: { requiresAuth: true },
    },
    {
      path: '/new',
      name: 'NewStory',
      component: NewStory,
      meta: { requiresAuth: true },
    },
    ...importRoutes,
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { requiresAuth: false },
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
      meta: { requiresAuth: false },
    },
    {
      path: '/resend-instructions',
      name: 'ResendConfirmation',
      component: ResendConfirmation,
      meta: { requiresAuth: false },
    },
    {
      path: '/password-reset/send',
      name: 'SendPasswordReset',
      component: SendPasswordReset,
      meta: { requiresAuth: false },
    },
    {
      path: '/password-reset/:token',
      name: 'PasswordReset',
      component: PasswordReset,
      meta: { requiresAuth: false },
      props: true,
    },
    {
      path: '/validate/:token',
      name: 'Validate',
      component: Validate,
      props: true,
      meta: { requiresAuth: false },
    },
    {
      path: '/profile/:slug',
      name: 'Profile',
      component: Profile,
      props: true,
    },
    {
      path: '/profile/:slug/edit',
      name: 'EditProfile',
      component: EditProfile,
      props: true,
      meta: { requiresAuth: true },
    },
    {
      path: '/connect',
      name: 'Connect',
      component: Connect,
    },
    {
      path: '/connect/callback',
      name: 'ConnectCallback',
      component: ConnectCallback,
    },
    {
      path: '/404',
      name: 'PageNotFound',
      component: PageNotFound,
      meta: { requiresAuth: false },
    },
    {
      path: '*',
      name: 'Fallback',
      redirect: '/404',
    },
  ],
});

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!await auth.isConnected()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;
