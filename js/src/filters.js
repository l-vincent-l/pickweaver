import Vue from 'vue';
import anchorme from 'anchorme';

Vue.filter('text_to_links', text => anchorme(text, { truncate: 40, attributes: [{ name: 'class', value: 'card-link' }] }));
