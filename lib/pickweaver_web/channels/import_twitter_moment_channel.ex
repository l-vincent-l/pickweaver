defmodule PickweaverWeb.ImportTwitterMomentChannel do
  @moduledoc """
  Channel to import Twitter Moments
  """
  use Phoenix.Channel
  alias Pickweaver.Services.Import.TwitterMoment
  alias Pickweaver.Stories
  alias Pickweaver.Stories.Story
  alias Pickweaver.Import
  alias Pickweaver.Imports
  alias Pickweaver.Repo
  alias Pickweaver.Accounts
  alias PickweaverWeb.ImportTwitterMomentChannel
  import Logger

  def join("import:twitter_moment:" <> username, _message, socket) do
    {:ok, socket}
  end

  def handle_in("import", %{"body" => body}, socket) do
    moment_to_import = body["moment"]
    |> save_moment(socket)
    |> process_moment(socket)
  end

  defp process_moment(moment, socket) do
    user = socket.assigns.user
    broadcast! socket, "start_processing", %{story: %{url: moment.url}}
    Task.start(ImportTwitterMomentChannel, :process_from_url, [self(), moment.url, user])
    receive do
      {:ok, story, url} ->
        remove_import(url)
        broadcast! socket, "processed_story", %{story: %{slug: story.slug, url: url, status: :ok}}
      {:error, url} ->
        remove_import(url)
        broadcast! socket, "failed_story", %{story: %{url: url, status: :failure}}
    end
    broadcast! socket, "finished", %{}
    {:reply, :ok, socket}
  end

  defp remove_import(url) do
    url
    |> Imports.get_import_by_url()
    |> Imports.delete_import()
  end

  def process_from_url(parent, url, user) do
    {status, params} = TwitterMoment.import_from_moment_url(url, user)
    case status do
      :ok ->
        params = Map.put(params, "account_id", user.id)
        case Stories.create_story(params) do
          {:ok, %Story{} = story} ->
            send(parent, {:ok, story, url})
          {:error} ->
            send(parent, {:error, url})
        end
      _ ->
        send(parent, {:error, url})
    end
  end

  defp save_moment(moment, socket) do
    user_id = socket.assigns.user.id
    attrs = %{"url" => moment, "account_id" => user_id}
    {:ok, import} = Imports.create_import(attrs)
    broadcast! socket, "saved_import", %{import: import}
    import
  end
end
