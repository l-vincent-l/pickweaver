defmodule PickweaverWeb.ImportTwitterTweetsChannel do
  @moduledoc """
  Channel to import Tweets
  """

  use Phoenix.Channel
  alias Pickweaver.Services.Import.TwitterTweets
  alias Pickweaver.Stories
  alias Pickweaver.Stories.Story
  alias Pickweaver.Import
  alias Pickweaver.Imports
  alias Pickweaver.Repo
  alias Pickweaver.Accounts
  alias PickweaverWeb.ImportTwitterTweetsChannel
  import Logger

  def join("import:twitter_tweets:" <> username, _message, socket) do
    {:ok, socket}
  end

  @doc """
  Handles an "import" socket with body that starts the import
  """
  @spec handle_in(String.t, map(), Socket.t) :: tuple()
  def handle_in("import", %{"body" => body}, socket) do
    Logger.debug(inspect body)
    body["tweet"]
    |> save_tweet_import(socket)
    |> process_tweet_and_responses(socket)
  end

  @spec process_tweet_and_responses(%Import{}, Socket.t) :: tuple()
  defp process_tweet_and_responses(tweet, socket) do
    user_id = socket.assigns.user.id
    Task.start(ImportTwitterTweetsChannel, :process_from_url, [self(), tweet.url, user_id])
    receive do
      {:ok, story, url} ->
        remove_import(url)
        broadcast! socket, "processed_story", %{story: %{slug: story.slug, url: url, status: :ok}}
      {:error, url, reason} ->
        remove_import(url)
        broadcast! socket, "failed_story", %{story: %{url: url, status: :failure, reason: reason}}
    end
    broadcast! socket, "finished", %{}
    {:reply, :ok, socket}
  end

  @spec remove_import(String.t) :: {:ok, %Import{}}
  defp remove_import(url) do
    url
    |> Imports.get_import_by_url()
    |> Imports.delete_import()
  end

  @doc """
  Processes the import from an URL and returns status to the parent process

  ## Examples

      iex> process_from_url(parent, "https://twitter.com/nitot/status/957963447513296897", 1)
      {:ok, %Story{}, "https://twitter.com/nitot/status/957963447513296897"}

      iex> process_from_url(parent, "toto", 1)
      {:error, "toto", :not_tweet}
  """
  @spec process_from_url(pid(), String.t, integer()) :: tuple()
  def process_from_url(parent, url, user_id) do
    Logger.debug("Processing from url")
    case TwitterTweets.import_from_twitter_url(url) do
      {:ok, story_params} ->
        story_params = Map.put(story_params, "account_id", user_id)
        Logger.debug("Story twitter tweets imported params")
        Logger.debug(inspect story_params)
        case Stories.create_story(story_params) do
          {:ok, %Story{} = story} ->
            send(parent, {:ok, story, url})
          {:error, _} ->
            send(parent, {:error, url, :story_not_created})
        end
      {:error, :not_tweet} ->
        send(parent, {:error, url, :not_tweet})
    end
  end

  @spec save_tweet_import(String.t, Socket.t) :: %Import{}
  defp save_tweet_import(tweet, socket) do
    user_id = socket.assigns.user.id
    attrs = %{"url" => tweet, "account_id" => user_id}
    {:ok, import} = Imports.create_import(attrs)
    broadcast! socket, "saved_import", %{import: import}
    import
  end
end
