defmodule PickweaverWeb.AccountController do
  use PickweaverWeb, :controller
  use PhoenixSwagger

  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.{Account}
  alias Pickweaver.Accounts.Service.{Activation, ResetPassword}

  action_fallback PickweaverWeb.FallbackController

  def index(conn, _params) do
    accounts = Accounts.list_accounts()
    render(conn, "index.json", accounts: accounts)
  end

  swagger_path :create do
    summary "Create account"
    description "Registers a new account from params"
    parameters do
      email :body, :string, "An email for the new account", required: true
      username :body, :string, "An username for the new account", required: true
      password :body, :string, "A password for the new account", required: true
    end
    response 201, "Ok", Schema.ref(:Account)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def create(conn, %{"email" => email, "username" => username, "password" => password}) do
    account_params = %{"email" => email, "username" => username, "password" => password}
    with {:ok, %Account{} = account} <- Accounts.create_account(account_params) do
      Activation.send_confirmation_email(account, "locale")
          # {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(account)
      conn
      |> put_status(:created)
      |> render("confirmation.json", %{account: account})
    end
  end

  swagger_path :validate do
    summary "Validate account"
    description "Validate an account from params"
    parameters do
      token :body, :string, "The validation token for the account", required: true
    end
    response 201, "Ok", Schema.ref(:Account)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def validate(conn, %{"token" => token}) do
    with {:ok, %Account{} = account} <- Activation.check_confirmation_token(token) do
      {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(account)
      conn
      |> put_resp_header("location", account_path(conn, :show, account))
      |> render("show_with_token.json", %{account: account, token: token})
    end
  end

  swagger_path :resend_confirmation do
    summary "Resend confirmation"
    description "Resend the confirmation email"
    parameters do
      email :body, :string, "The email to resend the validation token", required: true
    end
    response 201, "Ok", Schema.ref(:Account)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def resend_confirmation(conn, %{"email" => email}) do
    with {:ok, %Account{} = account} <- Accounts.find_by_email(email),
         false <- is_nil(account.confirmation_token),
           true <- Timex.before?(Timex.shift(account.confirmation_sent_at, hours: 1), DateTime.utc_now()) do
      Activation.resend_confirmation_email(account)
      render(conn, "confirmation.json", %{account: account})
    else
      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> json(%{"error" => "Unable to find an account with this email"})
      _ ->
        conn
        |> put_status(:not_found)
        |> json(%{"error" => "Unable to resend the validation token"})
    end
  end

  swagger_path :send_reset_password do
    summary "Send reset account password email"
    description "Ask for email reset account password"
    parameters do
      email :body, :string, "The validation token for the account", required: true
    end
    response 201, "Ok", Schema.ref(:Account)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def send_reset_password(conn, %{"email" => email}) do
    with {:ok, %Account{} = account} <- Accounts.find_by_email(email),
         {:ok, _} <- ResetPassword.send_password_reset_email(account) do
      render(conn, "password_reset.json", %{account: account})
    else
      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> json(%{"errors" => "Unable to find an account with this email"})
      {:error, :email_too_soon} ->
        conn
        |> put_status(:not_found)
        |> json(%{"errors" => "You requested a new reset password too early"})
    end
  end

  swagger_path :reset_password do
    summary "Reset account password"
    description "Reset an account password with a validation token"
    parameters do
      token :body, :string, "The validation token for the account", required: true
      password :body, :string, "The new password for the account", required: true
    end
    response 201, "Ok", Schema.ref(:Account)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def reset_password(conn, %{"password" => password, "token" => token}) do
    with {:ok, %Account{} = account} <- ResetPassword.check_reset_password_token(password, token) do
      {:ok, token, _claims} = PickweaverWeb.Guardian.encode_and_sign(account)
      conn
      |> put_resp_header("location", account_path(conn, :show, account))
      |> render("show_with_token.json", %{account: account, token: token})
    else
      {:error, :invalid_token} ->
        conn
        |> put_status(:not_found)
        |> json(%{"errors" => %{"token" => ["Wrong token for password reset"]}})
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(PickweaverWeb.ChangesetView, "error.json", changeset: changeset)
    end
  end

  swagger_path :show_current_account do
    summary "Show logged-in account"
    description "Shows the current logged-in account, based on the JWT Token used"
    response 200, "Ok", Schema.ref(:Account)
  end
  def show_current_account(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    render(conn, "show_private.json", account: account)
  end

  swagger_path :show do
    summary "Show account"
    description "Shows an account"
    parameters do
      username :path, :string, "The account's slug", required: true
    end
    response 200, "Ok", Schema.ref(:Account)
    response 404, "Not found", Schema.ref(:Error)
  end
  def show(conn, %{"username" => slug}) do
    with {:ok, %Account{} = account} <- Accounts.get_account_by_slug(slug) do
      render(conn, "show.json", account: account)
    else
      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> render(PickweaverWeb.ErrorView, :"404")
    end
  end

  swagger_path :update do
    summary "Update logged-in account"
    description "Update the current logged-in account's details"
    parameters do
      account :body, Schema.ref(:Account), "The account details to update"
    end
    response 200, "Ok", Schema.ref(:Account)
    response 404, "Not found", Schema.ref(:Error)
  end
  def update(conn, %{"account" => account_params}) do
    account = Guardian.Plug.current_resource(conn)

    with {:ok, %Account{} = account} <- Accounts.update_account(account, account_params) do
      render(conn, "show.json", account: account)
    end
  end

  swagger_path :delete do
    summary "Delete logged-in account"
    description "Delete the current logged-in account"
    parameters do
      username :path, :string, "The account's slug", required: true
    end
    response 204, "Ok", Schema.ref(:Account)
    response 401, "Unauthorized", Schema.ref(:Error)
    response 404, "Not found", Schema.ref(:Error)
  end
  def delete(conn, _params) do
    with account <- Guardian.Plug.current_resource(conn) do
      with {:ok, %Account{}} <- Accounts.delete_account(account) do
        send_resp(conn, :no_content, "")
      end
    end
  end

  def swagger_definitions do
    %{
      Account: swagger_schema do
        title "Account"
        description "An user account"
        properties do
          username :string, "The account's username", required: true
          slug :string, "The account's slugged username", required: true
          #email :string, "The account's email", required: true
          role :integer, "The account's role : simple user or admin", required: true
          display_name :string, "The displayed name of the account, or full name", required: false
          avatar :string, "The avatar URL of the account", required: false
          inserted_at :string, "When was the account initially created", format: "ISO-8601"
        end
        example %{
          username: "my username",
          slug: "my-username",
          role: 0
        }
      end,
      Error: swagger_schema do
        title "Errors"
        description "Error responses from the API"
        properties do
          error :string, "The message of the error raised", required: true
        end
      end
    }
  end
end
