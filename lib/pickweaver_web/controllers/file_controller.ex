defmodule PickweaverWeb.FileController do
  use PickweaverWeb, :controller

  alias Pickweaver.Files
  alias Pickweaver.Files.File, as: FileModel

  action_fallback PickweaverWeb.FallbackController

  def index(conn, _params) do
    files = Files.list_files()
    render(conn, "index.json", files: files)
  end

  def create(conn, %{"file" => file_params}) do
    params = Map.put(file_params, "account_id", Guardian.Plug.current_resource(conn).id)
    with {:ok, %FileModel{} = file} <- Files.create_file(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", file_path(conn, :show, file))
      |> render("show.json", file: file)
    end
  end

  def show(conn, %{"file" => uuid}) do
    file = Files.get_file_by_uuid!(uuid)
    case File.read(Path.join("upload", file.path)) do
      {:ok, binary} ->
        send_resp(conn, 200, binary)
      _ ->
        send_resp(conn, :not_found, "")
    end
  end

  def update(conn, %{"id" => id, "file" => file_params}) do
    file = Files.get_file!(id)

    with {:ok, %FileModel{} = file} <- Files.update_file(file, file_params) do
      render(conn, "show.json", file: file)
    end
  end

  def delete(conn, %{"file" => uuid}) do
    file = Files.get_file_by_uuid!(uuid)
    with {:ok, %FileModel{}} <- Files.delete_file(file) do
      send_resp(conn, :no_content, "")
    end
  end
end
