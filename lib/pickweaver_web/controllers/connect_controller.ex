defmodule PickweaverWeb.ConnectController do
  @moduledoc """
  Handles the calls to external services APIs to search for elements
  """

  use PickweaverWeb, :controller

  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.Token
  alias Pickweaver.Services.Parser.Tweet
  alias Pickweaver.Services.Finder.Twitter, as: TwitterFinder
  alias Pickweaver.Services.Connect.Twitter, as: TwitterConnect

  import Logger

  @doc """
  Returns twitter favourites
  """
  def twitter_fav(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    json(conn, %{tweets: TwitterFinder.twitter_fav(account)})
  end

  @doc """
  Returns tweets for search
  """
  def twitter_search(conn, %{"search" => search} = _params) do
    account = Guardian.Plug.current_resource(conn)
    json(conn, %{tweets: TwitterFinder.twitter_search(account, search)})
  end

  @doc """
  Returns tweets for profile
  """
  def twitter_profile(conn, %{"profile" => profile}) do
    account = Guardian.Plug.current_resource(conn)
    json(conn, %{tweets: TwitterFinder.twitter_profile(account, profile)})
  end

  @doc """
  Returns tweets for own profile
  """
  def twitter_profile(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    json(conn, %{tweets: TwitterFinder.twitter_profile(account)})
  end

  @doc """
  Returns if account has a valid Twitter token
  """
  def twitter_has_token(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    json(conn, TwitterConnect.twitter_has_token(account))
  end

  @doc """
  Proceed to Twitter Auth
  """
  def twitter_auth(conn, %{"callback" => callback}) do
    case TwitterConnect.twitter_auth(callback) do
      {:ok, authenticate_url} ->
        json(conn, %{authenticate_url: authenticate_url})
      _ ->
        json(conn, %{status: :error})
    end
  end

  @doc """
  Callback route called when Twitter Auth succeeds
  """
  def twitter_auth_callback(conn, %{"oauth_token" => oauth_token, "oauth_verifier" => oauth_verifier}) do
    json(conn, TwitterConnect.twitter_auth_callback(oauth_token, oauth_verifier, Guardian.Plug.current_resource(conn).id))
  end
end
