defmodule PickweaverWeb.StoryController do
  use PickweaverWeb, :controller
  use PhoenixSwagger

  import Logger
  alias Pickweaver.Stories
  alias Pickweaver.Stories.Story
  alias Pickweaver.Services.Import.Storify
  alias Pickweaver.Files.File
  alias Pickweaver.Files
  alias Pickweaver.Repo
  alias Pickweaver.Services.Parser

  action_fallback PickweaverWeb.FallbackController

  def index(conn, params) do
    page = Stories.list_stories(params)
    render_paginated(conn, page)
  end

  swagger_path :index_public_for_user do
    summary "List public stories for user"
    description "List paginated stories for a given user slug"
    parameters do
      username :path, :string, "The user's slug", required: true
      page :path, :integer, "The page of the user's stories", required: false
    end
    response 200, "Ok", Schema.ref(:Stories)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def index_public_for_user(conn, %{"username" => slug} = _params) do
    page = Stories.list_stories_for_account_slug(slug, _params)
    render_paginated(conn, page)
  end

  swagger_path :index_own_stories do
    summary "List own stories for user"
    description "List paginated stories for the authenticated user"
    parameters do
      page :path, :integer, "The page of the user's stories", required: false
      orderBy :path, :string, "The story parameter on which to sort stories", required: false
      direction :path, :string, "The direction (asc or desc) in which to return stories", required: false
      search :path, :string, "A string to search into story titles", required: false
    end
    response 200, "Ok", Schema.ref(:Stories)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  @default_own_index_params %{"page" => 1, "order_by" => "inserted_at", "direction" => "desc"}
  @default_search_params %{"search" => "", "page" => 1, "order_by" => "inserted_at", "direction" => "desc"}
  def index_own_stories(conn, params) do
    account = Guardian.Plug.current_resource(conn)
    stories = if is_nil Map.get(params, "search", nil) do
      Stories.list_stories_for_account(account, Map.merge(@default_own_index_params, params))
    else
      Stories.search_stories_for_account(account, Map.merge(@default_search_params, params))
    end
    render_paginated(conn, stories)
  end

  swagger_path :search do
    summary "Search published stories"
    description "Search through published stories inside their title"
    parameters do
      page :path, :integer, "The page of the user's stories", required: false
      orderBy :path, :string, "The story parameter on which to sort stories", required: false
      direction :path, :string, "The direction (asc or desc) in which to return stories", required: false
      search :path, :string, "A string to search into story titles", required: false
    end
    response 200, "Ok", Schema.ref(:Stories)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def search(conn, params) do
    page = Stories.search_stories(Map.merge(@default_search_params, params))
    render_paginated(conn, page)
  end

  defp render_paginated(conn, page) do
    conn
    |> Scrivener.Headers.paginate(page)
    |> render("index.json", stories: page.entries, page: %{page_number: page.page_number, page_size: page.page_size, total_pages: page.total_pages, total_entries: page.total_entries})
  end

  swagger_path :create do
    summary "Create story"
    description "Creates a new story from params"
    parameters do
      title :body, :string, "An title for the new story", required: true
      elements :body, :array, "An array of elements UUIDs for the new story", required: true
      description :body, :string, "A description for the new story", required: true
    end
    response 201, "Ok", Schema.ref(:Story)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def create(conn, story_params) do
    user = Guardian.Plug.current_resource(conn)
    params = story_params["story"] |> Map.put("account_id", user.id)
#    if params["media"] do
#      file_id = Files.get_file_by_uuid!(params["media"]).id
#      params = Map.put(params, "media_id", file_id)
#    end
    Logger.debug(inspect params)
    with {:ok, %Story{} = story} <- Stories.create_story(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", story_path(conn, :show, user.slug, story.slug))
      |> render("show.json", story: story)
    end
  end

  @doc """
  Finds a story and load it's first elements

  ## Examples

      iex> process_story("my-story", "thomas")
      {:commit, %Story{}}

      iex> process_story("inexistant", "nooo")
      {:ignore, nil}
  """
  @spec process_story(String.t, String.t, boolean) :: {atom(), Story.t}
  defp process_story(slug, username, load_all \\ true) do
    case Stories.get_story_by_slug_and_username(slug, username, 1, 20, load_all) do
      {:ok, %Story{} = story} ->
        Logger.debug("Fetched story")
        {:commit, story}
      _ ->
        {:ignore, nil}
    end
  end

  @doc """
  Fetch elements for story.

  Used to load more elements for a story
  """
  def fetch_elements_for_story(conn, %{"username" => username, "slug" => slug, "page" => page}) do
    {page, _} = Integer.parse(page)
    with {:ok, %Story{} = story} <- Stories.get_story_by_slug_and_username(slug, username, page) do
      render(conn, "elements.json", story: story.elements)
    end
  end

  @doc """
  Load a story from the cache or fetch it
  """
  @spec cache_story(String.t, String.t) :: {atom(), any()}
  defp cache_story(slug, username) do
    cache = Cachex.fetch(:my_cache, "#{username}:#{slug}", fn slugandusername ->
    [username, slug] = String.split(slugandusername,":")
      {status, story} = process_story(slug, username, false)
      case status do
        :commit ->
          {status, story}
        _ ->
          {:ignore, nil}
      end
    end)
    case cache do
      {success, result} when success in [:ok, :loaded, :commit] and result != nil ->
        {:ok, result}
      {error, nil} when error in [:loaded, :error, :ignore] ->
        {:error, :not_found}
    end
  end

  swagger_path :show do
    summary "Show a story"
    description "Shows a story.
    Stories (and their 20 first elements) are cached so that they're not fetched for database each time"
    parameters do
      slug :path, :string, "The story's slug", required: true
    end
    response 200, "Ok", Schema.ref(:Story)
    response 404, "Not found", Schema.ref(:Error)
  end
  @doc """
  Shows a story
  """
  def show(conn, %{"username" => username, "slug" => slug}) do
    with {:ok, %Story{} = story} <- cache_story(slug, username) do
      if story.published do
        render(conn, "show.json", story: story)
      end
    end
  end

  @doc """
  Returns a story with all it's elements.

  Used to get the edit pages and preview page
  """
  def show_all(conn, %{"username" => username, "slug" => slug}) do
    with {:commit, story} <- process_story(slug, username, true) do
      if story.account.id === Guardian.Plug.current_resource(conn).id do
        render(conn, "show.json", story: story)
      end
    end
  end


  # TODO : I shouldn't be here
  def render(conn, %{"url" => url}) do
    account = Guardian.Plug.current_resource(conn)
    conn
    |> put_resp_header("content-type", "application/json")
    |> resp(:ok, Phoenix.View.render_to_string(PickweaverWeb.StoryView, "element.json", %{element: Parser.process_single_url(url, account)}))
  end

#  defp show_by_id(conn, id) do
#    story = Stories.get_story!(id)
#    render(conn, "show.json", story: story)
#  end

  swagger_path :update do
    summary "Update story"
    description "Update a story's details"
    parameters do
      slug :path, :string, "The story's slug", required: true
      account :body, Schema.ref(:Story), "The story details to update"
    end
    response 200, "Ok", Schema.ref(:Story)
    response 404, "Not found", Schema.ref(:Error)
  end
  def update(conn, %{"username" => username, "slug" => slug, "story" => story_params}) do
    with {:ok, story} <- Stories.get_story_by_slug_and_username(slug, username) do
      story_account_id = story.account.id

#      if story_params["media"] do
#        Logger.debug("We have a media !")
#        file_id = Files.get_file_by_uuid!(story_params["media"]).id
#        story_params = Map.put(story_params, "media_id", file_id)
#        Logger.debug(inspect story_params)
#      end

      case Guardian.Plug.current_resource(conn).id do
        story_account_id ->
          with {:ok, %Story{} = story} <- Stories.update_story(story, story_params) do
            with {:commit, story} <- process_story(slug, username) do
              case Cachex.del(:my_cache, "#{username}:#{slug}") do
                {:ok, true} ->
                  render(conn, "show.json", story: story)
                _ ->
                  send_resp(conn, 500, "Cache issue")
              end
            end
          end
        _ ->
          send_resp(conn, 400, "This isn't your story !")
      end
    end
  end

  swagger_path :delete do
    summary "Delete story"
    description "Delete a story"
    parameters do
      slug :path, :string, "The user slugged username", required: true
      slug :path, :string, "The story's slug", required: true
    end
    response 204, "Ok", Schema.ref(:Story)
    response 401, "Unauthorized", Schema.ref(:Error)
    response 404, "Not found", Schema.ref(:Error)
  end
  def delete(conn, %{"username" => username, "slug" => slug}) do
    with {:ok, story} <- Stories.get_story_by_slug_and_username(slug, username) do
      if Guardian.Plug.current_resource(conn).id != story.account_id do
        send_resp(conn, 400, "This isn't your story !")
      else
        with {:ok, %Story{}} <- Stories.delete_story(story) do
          Cachex.del(:my_cache, "#{username}:#{slug}")
          send_resp(conn, :no_content, "")
        end
      end
    end
  end

  def swagger_definitions do
    %{
      Story: swagger_schema do
        title "Story"
        description "Represents a story"
        properties do
          title :string, "The story's title", required: true
          slug :string, "The story's slugged title", required: true
          description :string, "The story's description", required: false
          account Schema.ref(:Account), "The story's author's account", required: true
          inserted_at :string, "When was the story initially created", format: "ISO-8601"
          elements (Schema.new do
            description "Represents paginated elements for this story. At first we have 20 elements with the story, we must call /stories/:slug/:page to get more"
            properties do
             entries Schema.ref(:Elements), "A subset of elements for this story"
             total_entries :integer, "The total number of elements for this story"
             total_pages :integer, "The total number of pages of elements for this story"
             page_number :integer, "The index of the current page"
             page_size :integer, "The number of elements per page"
           end
          end)
        end
        example %{
          title: "my awesome story",
          slug: "my-awesome-story",
          description: "A story of myths and dragons"
        }
      end,
      Error: swagger_schema do
        title "Errors"
        description "Error responses from the API"
        properties do
          error :string, "The message of the error raised", required: true
        end
      end,
      Stories: swagger_schema do
        title "Stories"
        description "A collection of Stories"
        type :array
        items Schema.ref(:Story)
      end
    }
  end
end
