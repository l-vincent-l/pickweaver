defmodule PickweaverWeb.ImportController do
  use PickweaverWeb, :controller

  import Logger
  alias Pickweaver.Stories
  alias Pickweaver.Stories.Story
  alias Pickweaver.Services.Import
  alias Pickweaver.Imports
  alias Pickweaver.Services.Connect.Twitter
  alias Pickweaver.Services.Import.Storify

  action_fallback PickweaverWeb.FallbackController

  def index_for_user(conn, %{"userid" => userid}) do
    stories = Stories.list_stories(userid)
    render(conn, "index.json", stories: stories)
  end

  def import(conn, %{"url" => url} = _params) do
    account = Guardian.Plug.current_resource(conn)
    with %{status: :ok} <- Twitter.twitter_has_token(account) do
      Task.start(Storify, :import_from_url, [url, account, self()])
      receive do
        {:ok, story} ->
          story = Map.put(story, :elements, process_elements(story.elements))
          params = Map.put(story, :account_id, account.id)
          with {:ok, %Story{} = story} <- Stories.create_story(params) do
            conn
            |> put_status(:created)
            |> put_resp_header("location", story_path(conn, :show, story.account.slug, story.slug))
            |> put_view(PickweaverWeb.StoryView)
            |> render("show.json", story: story)
          end
        {:error, err} ->
          conn
          |> put_status(:not_found)
          |> json(%{error: err})
      end
    else
      %{status: :missing} ->
        conn
        |> put_status(:forbidden)
        |> json(%{error: "Missing twitter authentication"})
    end
  end

  defp process_elements(elements) do
    Enum.map(elements, fn element ->
      if !Map.has_key?(element, :data) do
        {:ok, element} = Stories.create_element(element)
        element.id
      else
        element.id
      end
    end)
  end

  def list_for_user(conn, _params) do
    with account <- Guardian.Plug.current_resource(conn) do
      imports = Imports.list_imports(account)
      render(conn, "imports.json", imports: imports)
    end
  end

  def retry(conn, %{"import" => import} = _params) do
    with account <- Guardian.Plug.current_resource(conn) do
      {import_id, _} = Integer.parse(import)
      import = Imports.get_import(import_id)
      Task.start(PickweaverWeb.ImportStorifyChannel, :process_import, [self(), import, account])
      receive do
        {:ok, story, url} ->
          remove_import(url)
          PickweaverWeb.StoryView.render("show.json", story: story)
        {:error, url} ->
          Logger.debug("error")
          json(conn, %{})
      end
    end
  end

  def retry_all(conn, _params) do
    with account <- Guardian.Plug.current_resource(conn) do
      imports = Imports.list_all_user_imports(account)
      Enum.each(imports, fn import ->
        mark_import_as(import, 1)
        Task.start(PickweaverWeb.ImportStorifyChannel, :process_import, [self(), import, account])
        receive do
          {:ok, story, url} ->
            remove_import(url)
          {:error, url} ->
            mark_import_as(import, 2)
        end
      end)
      json(conn, %{status: :processing})
    end
  end

  def remove_import(conn, %{"import" => import} = _params) do
    with account <- Guardian.Plug.current_resource(conn) do
      {import_id, _} = Integer.parse(import)
      with import <- Imports.get_import(import_id) do
        Imports.delete_import(import)
          json(conn, %{})
      end
    end
  end

  defp remove_import(url) do
    url
    |> Imports.get_import_by_url()
    |> Imports.delete_import()
  end

  defp mark_import_as(import, status) do
    args = %{status: status}
    if (status == 1) do
      Map.put(args, :tentatives, import.tentatives + 1)
    end
    Imports.update_import(import, args)
  end
end
