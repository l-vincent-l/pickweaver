defmodule PickweaverWeb.UploadController do
  use PickweaverWeb, :controller
  use ExTus.Controller
  alias Pickweaver.Files

  # start upload file callback
  def on_begin_upload(file_info) do
  end

  # Completed upload file callback
  def on_complete_upload(conn, file_info) do
    Files.create_file(%{"uuid" => file_info.identifier, "path" => file_info.filename, "account_id" => Guardian.Plug.current_resource(conn).id})
    perform_file_cmd(file_info.filename)
  end

  defp perform_file_cmd(file_path) do
    case System.cmd("file", ["--mime-type", "uploads/" <> file_path]) do
      {result, 0} ->
        extract_meta(result)
    end
  end

  defp extract_meta(result) do
    result
    |> String.trim
    |> String.split(": ")
    |> List.last
    |> (&%{"content_type" => &1}).()
  end
end
