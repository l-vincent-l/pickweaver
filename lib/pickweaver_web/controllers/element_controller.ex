defmodule PickweaverWeb.ElementController do
  use PickweaverWeb, :controller
  use PhoenixSwagger

  import Logger
  alias Pickweaver.Stories
  alias Pickweaver.Stories.Element
  alias Pickweaver.Services.Parser
  alias Pickweaver.Services.Parser.Text

  action_fallback PickweaverWeb.FallbackController

  swagger_path :create do
    summary "Create element"
    description "Creates a new element from params"
    parameters do
      type :body, :string, "A type of element, either plain text or an URL", required: true
      data :body, :string, "A string representing the data, either the text or the URL", required: true
    end
    response 201, "Ok", Schema.ref(:Element)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  @doc """
  Creates an element with data

  If the element has an URL and already exists, it will be directly returned
  """
  def create(conn, %{"type" => type, "data" => data}) do
    Logger.debug("Creating an element")
    Logger.debug(inspect data)
    account = Guardian.Plug.current_resource(conn)
    case type do
      "url" ->
        case Stories.get_element_by_url(data) do
          nil ->
            data
            |> Parser.process_single_url(account)
            |> createElement(conn)
          element ->
            showElement(conn, element)
        end
      "text" ->
        createElement(%Text{text: Parser.cleanHTML(data)}, conn)
    end
  end

  defp createElement(data, conn) do
    Logger.debug("creating element from data")
    Logger.debug(inspect data)
    with {:ok, %Element{} = element} <- Stories.create_element(data) do
      showElement(conn, element)
    end
  end

  defp showElement(conn, %Element{} = element) do
    Logger.debug("Calling showElement in element_controller")
    Logger.debug(inspect element)
    conn
    |> put_status(:created)
    |> put_resp_header("location", element_path(conn, :show, element))
    |> render("element.json", element: element)
  end

  swagger_path :show do
    summary "Show an element"
    description "Shows an element"
    parameters do
      id :path, :string, "The element's UUID", required: true
    end
    response 200, "Ok", Schema.ref(:Element)
    response 404, "Not found", Schema.ref(:Error)
  end
  @doc """
  Show an element identified by it's UUID
  """
  def show(conn, %{"id" => uuid}) do
    element = Stories.get_element!(uuid)
    render(conn, "element.json", element: element)
  end

  def swagger_definitions do
    %{
      Element: swagger_schema do
        title "Element"
        description "Represents a single element. Can be a tweet, some text, some youtube video, ..."
        properties do
          id :string, "The element's UUID", required: true
          url :string, "The element's URL if it's not a text element", required: false
          data :map, "The element's raw data", required: true
          type :enum, "The type of element", required: true
          inserted_at :string, "When was the element initially created", format: "ISO-8601"
        end
        example %{
          url: "https://twitter.com/nitot/status/957963447513296897",
          data: %{
            type: "tweet",
            user: %{},
            text: "tweet text",
            id: "Internal ID from the source, here Twitter ID",
            images: [],
            videos: [],
          },
          type: "tweet",
          id: "c8af9674-6ba4-49b1-bfe6-96d63596c6bf",
        }
      end,
      Elements: swagger_schema do
        title "Elements"
        description "A collection of Elements"
        type :array
        items Schema.ref(:Element)
      end,
      Error: swagger_schema do
        title "Errors"
        description "Error responses from the API"
        properties do
          error :string, "The message of the error raised", required: true
        end
      end
    }
  end
end
