defmodule PickweaverWeb.YoutubeView do
  alias Pickweaver.Services.Parser.Youtube
  alias PickweaverWeb.ViewHelpers

  use PickweaverWeb, :view
  import Logger

  def renderOpenGraph(%Youtube{} = youtube) do
    render("youtube.html", %{youtube: youtube})
  end

  def render("youtube.json", %{youtube: youtube}) do
    Logger.debug("showing youtube obj")
    Logger.debug(inspect youtube)
    %{
      type: "youtube",
      title: youtube["title"],
      description: youtube["description"],
      id: youtube["id"],
      image: youtube["thumbnails"]["high"]["url"],
      published_at: to_unix(youtube["published_at"]),
    }
  end

  def toLinks(text) do
    urls = Regex.scan(~r/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/iu, text, [capture: :first])
    urls
    |> processURLs()
    |> replaceURLs(text)
  end

  defp processURLs(urls) do
    Enum.map(urls, fn url -> %{url: url, html: processURL(url)} end)
  end

  def processURL([url]) do
    "<a class=\"card-link\" href=\"" <> url <> "\">" <> url <> "</a>"
  end

  defp replaceURLs([head | tail], body) do
    body = replaceURLs(tail, body)
    body = replaceURL(head, body)
    body
  end

  defp replaceURLs([], body) do
    body
  end

  defp replaceURL(url_map, body) do
    String.replace(body, hd(url_map.url), url_map.html)
  end

  def to_unix(string_date) do
    Logger.debug(inspect string_date)
    {:ok, datetime} = Timex.parse(string_date, "{ISO:Extended}")
    DateTime.to_unix(datetime)
  end
end
