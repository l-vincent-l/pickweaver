defmodule PickweaverWeb.SessionView do
  use PickweaverWeb, :view

  def render("token.json", %{token: token, user: user}) do
    %{token: token, user: render_one(user, PickweaverWeb.AccountView, "account.json")}
  end
end
