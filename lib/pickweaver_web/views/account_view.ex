defmodule PickweaverWeb.AccountView do
  use PickweaverWeb, :view
  alias PickweaverWeb.AccountView
  alias Pickweaver.Stories

  def render("index.json", %{accounts: accounts}) do
    %{accounts: render_many(accounts, AccountView, "account.json")}
  end

  def render("show.json", %{account: account}) do
    %{account: render_one(account, AccountView, "account.json")}
  end

  def render("show_private.json", %{account: account}) do
    %{account: render_one(account, AccountView, "account_private.json")}
  end

  def render("show_with_token.json", %{account: account, token: token}) do
    %{
      account: render_one(account, AccountView, "account.json"),
      token: token
    }
  end

  def render("account.json", %{account: account}) do
    %{id: account.id,
      username: HtmlSanitizeEx.strip_tags(account.username),
      slug: account.slug,
      role: account.role,
      display_name: HtmlSanitizeEx.strip_tags(if account.display_name do account.display_name else account.username end),
      avatar: account.avatar,
      stats: %{stories: Stories.count_published_stories_for_account(account)}
    }
  end

  def render("confirmation.json", %{account: account}) do
    %{
      email: account.email,
    }
  end

  def render("password_reset.json", %{account: account}) do
    %{
      email: account.email,
    }
  end

  def render("account_private.json", %{account: account}) do
    %{id: account.id,
      username: HtmlSanitizeEx.strip_tags(account.username),
      slug: account.slug,
      role: account.role,
      display_name: HtmlSanitizeEx.strip_tags(if account.display_name do account.display_name else account.username end),
      email: account.email,
      avatar: account.avatar,
    }
  end
end
