defmodule PickweaverWeb.StoryView do
  use PickweaverWeb, :view
  alias PickweaverWeb.StoryView
  alias PickweaverWeb.AccountView
  alias PickweaverWeb.FileView
  alias PickweaverWeb.ElementView
  import Logger
  alias PickweaverWeb.{TweetView, OpenGraphView, BasicView, YoutubeView}

  def render("index.json", %{stories: stories, page: page}) do
    %{
      stories: render_many(stories, StoryView, "story_simple.json"),
      page: page,
     }
  end

  def render("show.json", %{story: story}) do
    %{story: render_one(story, StoryView, "story.json")}
  end

  def render("story_simple.json", %{story: story}) do
    %{id: story.id,
      title: HtmlSanitizeEx.strip_tags(story.title),
      created_at: Timex.format!(story.inserted_at, "{ISO:Extended:Z}"),
      updated_at: Timex.format!(story.updated_at, "{ISO:Extended:Z}"),
      slug: story.slug,
      description: HtmlSanitizeEx.strip_tags(story.description),
      media: render_one(story.media, FileView, "file.json"),
      nb_elements: length(story.elements),
      user: render_one(story.account, AccountView, "account.json"),
      published: story.published,
      header: story.header,
    }
  end

  def render("story.json", %{story: story}) do
    %{id: story.id,
      title: HtmlSanitizeEx.strip_tags(story.title),
      created_at: Timex.format!(story.inserted_at, "{ISO:Extended:Z}"),
      updated_at: Timex.format!(story.updated_at, "{ISO:Extended:Z}"),
      slug: story.slug,
      description: HtmlSanitizeEx.strip_tags(story.description),
      media: render_one(story.media, FileView, "file.json"),
      elements: render_one(story.elements, StoryView, "elements.json"),
      user: render_one(story.account, AccountView, "account.json"),
      published: story.published,
      header: story.header,
    }
  end

  def render("elements.json", %{story: elements_paged}) do
    %{
      entries: render_many(elements_paged.entries, ElementView, "element.json"),
      total_pages: elements_paged.total_pages,
      total_entries: elements_paged.total_entries,
      page_number: elements_paged.page_number,
      page_size: elements_paged.page_size,
    }
  end

  def render("element_simple.json", %{story: element}) do
    element
  end
end
