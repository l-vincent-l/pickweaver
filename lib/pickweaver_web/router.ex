defmodule PickweaverWeb.Router do
  use PickweaverWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_auth do
    plug :accepts, ["json"]
    plug PickweaverWeb.AuthPipeline
  end

  scope "/api", PickweaverWeb do
    pipe_through :api

    scope "/v1" do
      # Register
      post "/users", AccountController, :create
      get "/users/validate/:token", AccountController, :validate
      post "/users/resend", AccountController, :resend_confirmation

      post "/users/password-reset/send", AccountController, :send_reset_password
      post "/users/password-reset/post", AccountController, :reset_password

      # View an user
      get "/users/:username", AccountController, :show

      # Login
      post "/login", SessionController, :sign_in

      # View a story
      get "/stories/:username/:slug", StoryController, :show

      # Get elements for a story
      get "/stories/:username/:slug/elements/:page", StoryController, :fetch_elements_for_story

      # Get stories for an user
      get "/users/:username/stories", StoryController, :index_public_for_user

      # Search stories, get best stories, ...
      get "/search/stories", StoryController, :search

      get "/proxy/:url/:signature", ProxyController, :proxify

    end
  end

   scope "/api", PickweaverWeb do
     pipe_through :api_auth

     scope "/v1" do
       # Logout
       post "/logout", SessionController, :sign_out

       # Get the authenticated user
       get "/user", AccountController, :show_current_account
       patch "/user", AccountController, :update
       put "/user", AccountController, :update
       delete "/user", AccountController, :delete

       # Operations on stories
       post "/stories", StoryController, :create
       patch "/stories/:username/:slug", StoryController, :update
       put "/stories/:username/:slug", StoryController, :update
       delete "/stories/:username/:slug", StoryController, :delete

       # Get a story with all it's elements
       get "/stories/:username/:slug/all", StoryController, :show_all

       # Get all stories for an user
       get "/user/stories", StoryController, :index_own_stories

       resources "/elements", ElementController, only: [:create, :show]

       post "/import/storify/url", ImportController, :import

       get "/imports", ImportController, :list_for_user
       get "/imports/retry", ImportController, :retry_all
       get "/imports/:import/retry", ImportController, :retry
       get "/imports/:import/delete", ImportController, :remove_import

       resources "/tags", TagsController, except: [:new, :edit]

       get "/connect/twitter/token", ConnectController, :twitter_has_token
       post "/connect/twitter", ConnectController, :twitter_auth
       post "/connect/twitter/callback", ConnectController, :twitter_auth_callback

       get "/finder/twitter/favourites", ConnectController, :twitter_fav
       get "/finder/twitter/profile/:profile", ConnectController, :twitter_profile
       get "/finder/twitter/profile", ConnectController, :twitter_profile
       post "/finder/twitter/search", ConnectController, :twitter_search
     end
  end

  scope "/api/v1/doc" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :pickweaver, swagger_file: "swagger.json"
  end

  scope "/files", PickweaverWeb do
    pipe_through :api_auth
    options "/",  			UploadController, :options
    options "/:file",  		UploadController, :options
    match :head, "/:file",  UploadController, :head
    post "/",  				UploadController, :post
    patch "/:file",  		UploadController, :patch
    delete "/:file",  		UploadController, :delete
  end

  scope "/files", PickweaverWeb do
    pipe_through :api
    get "/:file", FileController, :show
  end

  if Mix.env == :dev do
    # If using Phoenix
    forward "/sent_emails", Bamboo.EmailPreviewPlug
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Pickweaver"
      }
    }
  end
end
