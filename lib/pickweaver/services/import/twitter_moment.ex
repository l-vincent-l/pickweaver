defmodule Pickweaver.Services.Import.TwitterMoment do
  @moduledoc """
  Import a Twitter Moment
  """
  alias Pickweaver.Services.Import
  alias Pickweaver.Services.Parser.Tweet
  alias Pickweaver.Accounts.Account
  import Meeseeks.XPath
  import Logger

  @doc """
  Import tweets and metadata from a Twitter Moment URL
  """
  @spec import_from_moment_url(String.t, Account.t) :: any()
  def import_from_moment_url(url, %Account{} = account) do
    if String.match?(url, ~r/^https:\/\/twitter\.com\/i\/moments\//iu) do
      case HTTPoison.get url do
        {:ok, response} ->
          import_tweets_from_moment_html(response.body, account)
        {:error, %HTTPoison.Error{id: nil, reason: :timeout}} ->
          import_from_moment_url(url, account)
      end
    else
      {:error, :not_moment}
    end
  end

  @doc """
  Import tweets and metadata directly from a Twitter Moment html.
  """
  @spec import_tweets_from_moment_html(String.t, Account.t) :: map()
  def import_tweets_from_moment_html(body, %Account{} = account) do
    document = Meeseeks.parse(body)
    elements = document
      |> Meeseeks.all(xpath("//div[contains(@class, 'MomentCapsuleItem')]"))
      |> Enum.map(fn element ->
        if Meeseeks.attr(element, "data-permalink-path") != nil do
          "https://twitter.com#{Meeseeks.attr(element, "data-permalink-path")}"
        end
      end)
    |> Enum.filter(& !is_nil(&1))
    |> Enum.uniq()
    Logger.debug(inspect elements)
    elements = process_elements(elements, account)
    Logger.debug(inspect elements)
    elements = Enum.map(elements, fn element -> element.id end)
    Logger.debug("Finished producing elements")
    Logger.debug(inspect elements)
    {:ok, %{
      "title" => Meeseeks.text(Meeseeks.one(document, xpath("//h1[contains(@class, 'MomentCapsuleCover-title')]"))),
      "elements" => elements,
      "description" => Meeseeks.text(Meeseeks.one(document, xpath("//div[contains(@class, 'MomentCapsuleCover-description')]"))),
    }}
  end

  @spec process_elements(list(), Account.t) :: list(%Pickweaver.Stories.Element{})
  defp process_elements(tweets, %Account{} = account) do

    tweets
    |> Tweet.parse_twitter_url_list(account)
    |> Pickweaver.Stories.create_elements()
  end

  @spec process_elements([]) :: []
  defp process_elements([]) do
    []
  end
end
