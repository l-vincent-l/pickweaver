defmodule Pickweaver.Services.Import.Storify do
  @moduledoc """
  Imports data from Storify stories.
  """
  @stories_per_page 50
  alias Pickweaver.Services.Import
  import Logger
  import Meeseeks.XPath
  alias Pickweaver.Services.Parser.Element
  alias Pickweaver.Services.Parser
  alias Pickweaver.Accounts.Account

  @doc """
  Creates a story by importing a Storify story, using Storify's private API (while it lasts)

  The URL can either be the URL to the Story or the URL to it's XML representation

  ## Examples

      iex> import_from_url("https://storify.com/clarabdx/madeleineproject-saison-5")
      %{title: "#Madeleineproject SAISON 5", description: "Production Lumento avec la Radio Nova", elements: []}
  """
  @spec import_from_url(String.t, Account.t, pid()) :: map()
  def import_from_url(url, account, pid \\ self()) do
    if String.match?(url, ~r/(https:\/\/)?storify\.com\/[a-z0-9\-_]+(\/[a-z0-9\-_]+)?/iu) do
      if String.match?(url, ~r/(\.xml$)/iu) do
        SimpleHttp.start
        case SimpleHttp.get url do
          {:ok, response} ->
            import_from_xml(response.body, account, pid)
          {:error, reason} ->
            Logger.error(inspect reason)
        end
      else
        import_from_url(url <> ".xml", account, pid)
      end
    else
      {:error, :not_storify}
    end
  end

  @doc """
  Imports all stories made by a Storify account

  ## Examples

      iex> import_from_account_name("clarabdx")
      [%{title: "#Madeleineproject SAISON 5", description: "Production Lumento avec la Radio Nova", elements: []}]
  """
  @spec import_from_account_name(pid(), String.t, Account.t) :: list(map())
  def import_from_account_name(pid, storify_account_name, %Account{} = account) do
    stories_url = list_from_account_name(storify_account_name, pid)
    import_from_urls(pid, stories_url, account)
  end

  @doc """
  Import from URLs
  """
  @spec import_from_urls(pid(), list(String.t), Account.t) :: list(map())
  defp import_from_urls(pid, [head|tail], account) do
    Logger.debug("Importing urls")
    [import_from_url(head.url, account)] ++ import_from_urls(pid, tail, account)
  end

  defp import_from_urls(pid, [], account) do
    []
  end

  @doc """
  Lists stories made by a Storify account
  """
  @spec list_from_account_name(String.t, pid()) :: list(map())
  def list_from_account_name(account_name, pid \\ self()) do
    fetch_stories(account_name, pid)
  end

  @doc """
  Use Storify's private API to fetch stories
  """
  @spec fetch_stories(String.t, pid(), integer()) :: list(map())
  defp fetch_stories(account_name, pid \\ self(), page \\ 1) do
    Logger.debug("Finding page " <> Integer.to_string(page))
    SimpleHttp.start
    {:ok, response} = SimpleHttp.get "https://private-api.storify.com/v1/stories/#{account_name}?page=#{page}&per_page=#{@stories_per_page}&sort=date.created&filter=published"
    case find_stories(response.body) do
      {:ok, stories} ->
        send(pid, {:fetched_page, stories, page})
        fetch_stories(account_name, pid, page + 1)
      {:end, stories} ->
        send(pid, {:finished, stories, page})
        stories
    end
  end

  @doc """
  Find stories inside each page of the private API
  """
  @spec find_stories(String.t) :: {atom(), map()}
  defp find_stories(body) do
    content = Poison.decode!(body, keys: :atoms)
    stories = content.content.stories
              |> Enum.map(fn story -> %{
                                        url: story.permalink,
                                        title: :erlang.list_to_binary(:unicode.characters_to_list(story.title)),
                                        import: true,
                                        thumbnail: story.thumbnail,
                                        description: :erlang.list_to_binary(:unicode.characters_to_list(story.description)),
                                        status: 0,
                                      } end)
    Logger.debug(inspect stories)
    if ((length stories) < content.content.per_page) do
      {:end, stories}
    else
      {:ok, stories}
    end
  end

  @doc """
  Import elements from XML data
  """
  @spec import_from_xml(String.t, Account.t, pid()) :: map()
  defp import_from_xml(body, %Account{} = account, pid \\ self()) do
    body = :erlang.list_to_binary(:unicode.characters_to_list(body))
    document = Meeseeks.parse(body, :xml)
    elements = Meeseeks.all(document, xpath("/story/elements/item"))
    story = %{
      title: get_title(document),
      elements: process_elements(elements, account),
      description: Meeseeks.text(Meeseeks.one(document, xpath("/story/description"))),
    }
    send(pid, {:ok, story})
    story
  end

  defp get_title(document) do
    title = Meeseeks.text(Meeseeks.one(document, xpath("/story/title")))
    case title do
      "" ->
        Meeseeks.text(Meeseeks.one(document, xpath("/story/slug")))
      _ ->
        title
    end
  end

  @doc """
  Process multiple elements from Storify
  """
  @spec process_elements(list(), Account.t) :: list(%Element{})
  defp process_elements(elements, account) do
    elements
    |> Enum.map(fn element ->
      type = Meeseeks.text(Meeseeks.one(element, xpath("//type")))
      path = if type == "text" do
        "data/text"
      else
        "permalink"
      end
      element
      |> Meeseeks.one(xpath("//#{path}"))
      |> Meeseeks.text()
    end)
    |> Parser.process_multiple_urls(account)
  end

  @spec process_elements([]) :: []
  defp process_elements([]) do
    []
  end
end
