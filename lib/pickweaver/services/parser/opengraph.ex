defmodule Pickweaver.Services.Parser.Opengraph do
  @moduledoc """
  Parser that tries to fetch opengraph tags, but can use basic tags as a fallback
  """
  import Logger
  import Meeseeks.XPath
  alias Pickweaver.Services.Parser.Opengraph
  alias Pickweaver.Services.Parser.ParserError

  defstruct [:title, :description, :author, :url, :image, :force_url]
  @type t :: %Opengraph{title: String.t, description: String.t, author: String.t, image: String.t, force_url: String.t, url: String.t}

  @doc """
  Processes OpenGraph tags, and if that fails, calls a more basic parser


  ## Examples

      iex> parse_url("https://nextcloud.com/")
      %Opengraph{}

      iex> parse_url("https://mywebsite.tld", "<!DOCTYPE html><html><head><title>Hey you !</title><meta property='og:type' content='website' /><meta property='og:url' content='https://mywebsite.tld' /><meta property='og:title' content='My website' /></head></html>")
      %Opengraph{}
  """
  @spec parse_url(String.t, String.t) :: {:ok, Opengraph.t}
  def parse_url(url, html \\ nil) do
    html = if html == nil do
      fetch_html(url)
    else
      html
    end
    case String.valid?(html) do
      true ->
        case parse_open_graph_HTML(url, html) do
          {:ok, element} ->
            {:ok, element}
          {:error, nil} ->
            parse_basic_HTML(url, html)
        end
      false ->
        raise ParserError, message: "html encoding issue"
    end
  end

  @doc """
  Processes multiple URLs

  ## Examples

      iex> parse_urls(["https://trakt.tv/users/tcit", "https://framagit.org/tcit"])
      [%Opengraph{}, %Opengraph{}]
  """
  @spec parse_urls([]) :: []
  def parse_urls([]), do: []
  @spec parse_urls(list(String.t)) :: list(Opengraph.t)
  def parse_urls([head|tail]) do
    {:ok, element} = parse_url(head)
    [element] ++ parse_urls(tail)
  end

  @spec fetch_html(String.t) :: String.t
  defp fetch_html(url) do
    {:ok, response} = HTTPoison.get(url, [], [ssl: [{:versions, [:'tlsv1.2']}], hackney: [follow_redirect: true]])
    {"Content-Type", content_type} = List.keyfind(response.headers, "Content-Type", 0)
    case content_type do
      "text/html" <> _ ->
        response.body
      "image" <> _ ->
        Logger.debug("Found an image")
    end
  end

  @doc """
  Parses some HTML with `OpenGraphExtended`

  ## Examples

      iex> parse_open_graph_html("https://mywebsite.tld", "<!DOCTYPE html><html><head><title>Hey you !</title><meta property='og:type' content='website' /><meta property='og:url' content='https://mywebsite.tld' /><meta property='og:title' content='My website' /></head></html>")
      %Opengraph{}

      iex> parse_open_graph_html("https://mywebsite.tld", "<!DOCTYPE html><html><head><title>Hey you !</title></head></html>")
      {:error, nil}

  """
  @spec parse_open_graph_HTML(String.t, String.t) :: {:ok, Opengraph.t}
  defp parse_open_graph_HTML(url, html) do
    Logger.debug("Using OpenGraph Parser")
    open_graph_data = OpenGraphExtended.parse(html)
    if open_graph_data.url == nil do
      open_graph_data = %{open_graph_data | url: url}
    end
    Logger.debug(inspect open_graph_data)
    title = if String.valid?(open_graph_data.title), do: sanitize(to_string open_graph_data.title), else: nil
    description = if String.valid?(open_graph_data.description), do: sanitize(to_string open_graph_data.description), else: nil
    case title || description do
      nil ->
        Logger.debug("Opengraph data parsing failed")
        {:error, nil}
      _ ->
        {:ok, %Opengraph{
          title: title,
          description: description,
          url: open_graph_data.url,
          force_url: url,
          image: PickweaverWeb.ProxyController.gen_url(open_graph_data.image)
        }}
    end
  end

  @doc """
  Parses some HTML with basic parser.

  Fetches the following informations :

  * title from `<title>` tag
  * description from `<meta name="description">` tag
  * author from `<meta name="author">` tag
  * ...
  """
  @spec parse_basic_HTML(String.t, String.t) :: {:ok, Opengraph.t}
  defp parse_basic_HTML(url, html) do
    Logger.debug("Using Basic Parser for url")
    Logger.debug(url)
    case Meeseeks.parse(html) do
      {:error, error} ->
        raise ParserError, message: error
      document ->
        title = parse_title(document)
        description = parse_description(document)
        author = parse_author(document)
        {:ok, %Opengraph{
          title: title,
          description: description,
          author: author,
          url: find_url(document, url),
          force_url: url,
        }}
    end
  end

  @spec parse_title(Meeseeks.Document.t) :: String.t
  defp parse_title(document) do
    case Meeseeks.all(document, xpath("//title")) |> Enum.at(0) do
      {:error, error} ->
        raise ParserError, message: error
      text ->
        sanitize(Meeseeks.text(text))
    end
  end

  @spec parse_description(Meeseeks.Document.t) :: String.t
  defp parse_description(document) do
    case Meeseeks.all(document, xpath("//meta[@name='description']")) |> Enum.at(0) do
      {:error, error} ->
        raise ParserError, message: error
      text ->
        sanitize(Meeseeks.attr(text, "content"))
    end
  end

  @spec parse_author(Meeseeks.Document.t) :: String.t
  defp parse_author(document) do
    case Meeseeks.all(document, xpath("//meta[@name='author']")) |> Enum.at(0) do
      {:error, error} ->
        raise ParserError, message: error
      text ->
        sanitize(Meeseeks.attr(text, "content"))
    end
  end

  @spec find_url(Meeseeks.Document.t, String.t) :: String.t
  defp find_url(document, url) do
    case Meeseeks.attr(Meeseeks.one(document, xpath("//meta[@name='url']")), "content") do
      nil ->
        url
      url ->
        url
    end
  end

  @spec sanitize(nil) :: nil
  defp sanitize(nil) do
    nil
  end

  @spec sanitize(String.t) :: String.t
  defp sanitize(text) do
    HtmlEntities.decode(text)
  end
end
