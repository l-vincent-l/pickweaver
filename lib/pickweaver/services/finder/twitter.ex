defmodule Pickweaver.Services.Finder.Twitter do

  alias Pickweaver.Services.Connect.Twitter
  alias Pickweaver.Services.Parser.Tweet, as: TweetParser
  alias ExTwitter.Model.Tweet
  alias Pickweaver.Accounts.Account

  @doc """
  Returns twitter favourites
  """
  @spec twitter_fav(Account.t) :: list(Tweet.t)
  def twitter_fav(%Account{} = account) do
    Twitter.configure_ex_twitter(account)
    ExTwitter.favorites()
    |> Enum.map(fn tweet -> process_simple_tweet(TweetParser.process_tweet(tweet)) end)
  end

  @doc """
  Returns tweets for search
  """
  @spec twitter_search(Account.t, String.t) :: list(Tweet.t)
  def twitter_search(%Account{} = account, search) do
    Twitter.configure_ex_twitter(account)
    ExTwitter.search(search, [count: 20])
    |> Enum.map(fn tweet -> process_simple_tweet(TweetParser.process_tweet(tweet)) end)
  end

  @doc """
  Returns tweets for profile
  """
  @spec twitter_profile(Account.t, String.t) :: list(Tweet.t)
  def twitter_profile(%Account{} = account, profile) do
    Twitter.configure_ex_twitter(account)
    do_twitter_profile(profile)
  end

  @doc """
  Returns tweets for own profile
  """
  @spec twitter_profile(Account.t) :: list(Tweet.t)
  def twitter_profile(%Account{} = account) do
    account
    |> Twitter.configure_ex_twitter()
    |> do_twitter_profile()
  end

  @spec do_twitter_profile(String.t) :: list(Tweet.t)
  defp do_twitter_profile(profile) do
    ExTwitter.user_timeline([screen_name: profile, count: 20])
    |> Enum.map(fn tweet -> process_simple_tweet(TweetParser.process_tweet(tweet)) end)
  end

  @spec process_simple_tweet(%TweetParser{}) :: %TweetParser{}
  defp process_simple_tweet(%TweetParser{} = tweet) do
    Map.put(tweet, :user, %{username: tweet.user.name, screen_name: tweet.user.screen_name, profile: tweet.user.profile_image_url})
  end
end
