defmodule Pickweaver.Stories.Story do
  @moduledoc """
  Represents a story
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Logger
  alias Pickweaver.Stories.{Story, Tag, TitleSlug}
  alias Pickweaver.Accounts.Account
  alias Pickweaver.Files.File

  schema "stories" do
    field :title, :string
    field :description, :string
    field :elements, {:array, Ecto.UUID}
    field :elements_entities, {:array, Story}, virtual: true
    field :header, :string
    field :published, :boolean
    belongs_to :account, Account
    has_many :tags, Tag
    belongs_to :media, File

    field :slug, TitleSlug.Type

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(%Story{} = story, attrs) do
    story
    |> cast(attrs, [:title, :elements, :description, :account_id, :media_id, :published, :header])
    |> cast_assoc(:tags)
    |> validate_required([:title, :elements, :account_id])
    |> TitleSlug.maybe_generate_slug()
    |> TitleSlug.unique_constraint()
  end
end
