defmodule Pickweaver.Stories.Tag do
  @moduledoc """
  Represents a tag for a story
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Pickweaver.Stories.{Tag}

  @timestamps_opts updated_at: false
  schema "tags" do
    field :slug, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(%Tag{} = tag, attrs) do
    tag
    |> cast(attrs, [:slug, :name])
    |> validate_required([:slug, :name])
  end
end
