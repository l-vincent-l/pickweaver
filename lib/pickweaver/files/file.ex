defmodule Pickweaver.Files.File do
  @moduledoc """
  Represents a file (uploaded)
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Pickweaver.Files.File
  alias Pickweaver.Accounts.Account

  schema "files" do
    field :uuid, :string
    field :path, :string
    field :name, :string
    belongs_to :account, Account

    timestamps()
  end

  @doc false
  def changeset(%File{} = file, attrs) do
    file
    |> cast(attrs, [:uuid, :path, :name, :account_id])
    |> validate_required([:uuid, :path, :account_id])
  end
end
