defmodule Pickweaver.Imports do
  @moduledoc """
  The Import context.
  """

  import Ecto.Query, warn: false
  import Logger
  alias Pickweaver.Repo

  alias Pickweaver.Import
  alias Pickweaver.Accounts.Account

  @doc """
  Returns the list of imports.

  ## Examples

      iex> list_imports()
      [%import{}, ...]

  """
  def list_imports do
    Repo.all from s in Import,
      preload: [:account]
  end

  def list_imports_waiting(%Account{} = account) do
    Repo.all from s in Import,
      where: s.account_id == ^account.id and s.status == 0,
      preload: [:account]
  end

  def list_all_user_imports(%Account{} = account) do
    Repo.all from s in Import,
             where: s.account_id == ^account.id,
             preload: [:account]
  end

  @doc """
  Returns the list of imports for an account
  """
  @spec list_imports(Account.t) :: list(Import.t)
  def list_imports(%Account{} = account) do
    Repo.all from s in Import,
       where: s.account_id == ^account.id,
       preload: [:account]
  end

  @doc """
  Returns the list of imports for an account ID
  """
  @spec list_imports(integer()) :: list(Import.t)
  def list_imports(account_id) do
    Repo.all from s in Import,
      where: s.account_id == ^account_id,
      preload: [:account]
  end

  @spec get_import(integer) :: Import.t
  def get_import(import_id) do
    import = Repo.get!(Import, import_id)
    Repo.preload import, :account
  end

  @doc """
  Returns an import by it's URL
  """
  @spec get_import_by_url(String.t) :: Import.t
  def get_import_by_url(url) do
    import = Repo.get_by!(Import, url: url)
    Repo.preload import, :account
  end


  @doc """
  Creates a import.

  ## Examples

      iex> create_import(%{field: value})
      {:ok, %Import{}}

      iex> create_import(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_import(attrs \\ %{}) do
    case %Import{} |> Import.changeset(attrs) |> Repo.insert() do
      {:ok, import} ->
        {:ok, Repo.preload(import, :account)}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates an import.

  ## Examples

      iex> update_import(import, %{field: new_value})
      {:ok, %Import{}}

      iex> update_import(import, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_import(%Import{} = import, attrs) do
    import
    |> Import.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a import.

  ## Examples

      iex> delete_import(import)
      {:ok, %Import{}}

      iex> delete_import(import)
      {:error, %Ecto.Changeset{}}

  """
  def delete_import(%Import{} = import) do
    Repo.delete(import)
  end
end
