defmodule Pickweaver.Accounts.Token do
  @moduledoc """
  Represents an Auth Token for external services
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Pickweaver.Accounts.{Account, Token}

  schema "token" do
    field :service, :string
    field :data, :map
    belongs_to :account, Account

    timestamps()
  end

  @doc false
  def changeset(%Token{} = token, attrs) do
    token
    |> cast(attrs, [:service, :data, :account_id])
    |> validate_required([:service, :data, :account_id])
  end
end
