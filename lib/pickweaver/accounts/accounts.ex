defmodule Pickweaver.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Pickweaver.Repo

  alias Pickweaver.Accounts.{Account, Token}

  @doc """
  Returns the list of accounts.

  ## Examples

      iex> list_accounts()
      [%Account{}, ...]

  """
  def list_accounts do
    Repo.all(Account)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id)

  @doc """
  Gets a single account by it's slug
  """
  @spec get_account_by_slug(String.t) :: tuple()
  def get_account_by_slug(slug) do
    case Repo.get_by(Account, slug: slug) do
      nil -> {:error, :not_found}
      account -> {:ok, account}
    end
  end

  @doc """
  Gets a single account by it's slug

  Raises `Ecto.NoResultsError` if the Account does not exist.
  """
  def get_account_by_slug!(slug) do
    Repo.get_by!(Account, slug: slug)
  end

  @spec find_by_email(String.t) :: tuple
  def find_by_email(email) do
    case Repo.get_by(Account, email: email) do
      nil ->
        {:error, :not_found}
      account ->
        {:ok, account}
    end
  end

  @spec find_by_username(String.t) :: tuple
  def find_by_username(username) do
    case Repo.get_by(Account, username: username) do
      nil ->
        {:error, :not_found}
      account ->
        {:ok, account}
    end
  end

  @doc """
  Authenticate user
  """
  def authenticate(%{account: account, password: password}) do
    # Does password match the one stored in the database?
    case Comeonin.Argon2.checkpw(password, account.password_hash) do
      true ->
        # Yes, create and return the token
        PickweaverWeb.Guardian.encode_and_sign(account)
      _ ->
        # No, return an error
        {:error, :unauthorized}
    end
  end

  @doc """
  Creates a account.

  ## Examples

      iex> create_account(%{field: value})
      {:ok, %Account{}}

      iex> create_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_account(attrs \\ %{}) do
    %Account{}
    |> Account.registration_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a account.

  ## Examples

      iex> update_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_account(%Account{} = account, attrs) do
    account
    |> Account.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Account.

  ## Examples

      iex> delete_account(account)
      {:ok, %Account{}}

      iex> delete_account(account)
      {:error, %Ecto.Changeset{}}

  """
  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{source: %Account{}}

  """
  def change_account(%Account{} = account) do
    Account.changeset(account, %{})
  end

  def create_token(attrs \\ %{}) do
    case Repo.get_by(Token, service: attrs["service"], account_id: attrs["account_id"]) do
      nil -> %Token{}
      token -> token
    end
    |> Token.changeset(attrs)
    |> Repo.insert_or_update()
  end

  @spec get_token(Account.t, String.t) :: map()
  def get_token(%Account{} = account, service) do
    case Repo.get_by(Token, %{account_id: account.id, service: service}) do
      nil ->
        {:error, nil}
      token ->
        {:ok, token}
    end
  end
end
