defmodule Pickweaver.Accounts.Service.Activation do
  @moduledoc false

  alias Pickweaver.{Mailer, Repo, Accounts.Account, Accounts}
  alias PickweaverWeb.Endpoint
  alias Pickweaver.Email.Account, as: AccountEmail
  alias Ecto.{Changeset, Multi}

  require Logger

  import Ecto
  import Ecto.Query

  @doc false
  def check_confirmation_token(token) when is_binary(token) do
    with %Account{} = account <- Repo.get_by(Account, confirmation_token: token) do
      Accounts.update_account(account, %{"confirmed_at" => DateTime.utc_now(), "confirmation_sent_at" => nil, "confirmation_token" => nil})
    else
      err ->
        {:error, "Invalid token"}
    end
  end

  def resend_confirmation_email(%Account{} = account, locale \\ "en") do
    {:ok, account} = Accounts.update_account(account, %{"confirmation_sent_at" => DateTime.utc_now()})
    send_confirmation_email(account, locale)
  end

  def send_confirmation_email(%Account{} = account, locale \\ "en") do
    account
    |> AccountEmail.confirmation_email(locale)
    |> Mailer.deliver_later()
  end
end
