defmodule Pickweaver.Accounts.Service.ResetPassword do
  @moduledoc false

  alias Pickweaver.{Mailer, Repo, Accounts.Account, Accounts}
  alias PickweaverWeb.Endpoint
  alias Pickweaver.Email.Account, as: AccountEmail
  alias Ecto.{Changeset, Multi}

  require Logger

  import Ecto
  import Ecto.Query

  @doc """
  Check that the provided token is correct and update provided password
  """
  @spec check_reset_password_token(String.t, String.t) :: tuple
  def check_reset_password_token(password, token) do
    with %Account{} = account <- Repo.get_by(Account, reset_password_token: token) do
      Account.password_reset_changeset(account, %{"password" => password, "reset_password_sent_at" => nil, "reset_password_token" => nil}) |> Repo.update()
    else
      err ->
        {:error, :invalid_token}
    end
  end

  @doc """
  Send the email reset password, if it's not too soon since the last send
  """
  @spec send_password_reset_email(Account.t, String.t) :: tuple
  def send_password_reset_email(%Account{} = account, locale \\ "en") do
    with :ok <- we_can_send_email(account),
         {:ok, %Account{} = account_updated} <- Account.send_password_reset_changeset(account, %{"reset_password_token" => random_string(30), "reset_password_sent_at" => DateTime.utc_now()}) |> Repo.update() do
      mail = account_updated
      |> AccountEmail.reset_password_email(locale)
      |> Mailer.deliver_later()
      {:ok, mail}
    else
      {:error, reason} -> {:error, reason}
    end
  end

  @spec random_string(integer) :: String.t
  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64
  end

  @spec we_can_send_email(Account.t) :: boolean
  defp we_can_send_email(%Account{} = account) do
    case account.reset_password_sent_at do
      nil ->
        :ok
      _ ->
        case Timex.before?(Timex.shift(account.reset_password_sent_at, hours: 1), DateTime.utc_now()) do
          true ->
            :ok
          false ->
            {:error, :email_too_soon}
        end
      end
  end
end
