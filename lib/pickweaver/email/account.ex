defmodule Pickweaver.Email.Account do

  alias Pickweaver.Accounts.Account

  import Bamboo.Email
  import Bamboo.Phoenix
  use Bamboo.Phoenix, view: Pickweaver.EmailView
  import PickweaverWeb.Gettext

  def confirmation_email(%Account{} = account, locale \\ "en") do
    Gettext.put_locale(locale)
    instance_url = get_config(:instance)
    base_email()
    |> to(account.email)
    |> subject(gettext "Peakweaver: Confirmation instructions for %{instance}", instance: instance_url)
    |> put_header("Reply-To", get_config(:reply_to))
    |> assign(:token, account.confirmation_token)
    |> assign(:instance, instance_url)
    |> render(:registration_confirmation)
  end

  def reset_password_email(%Account{} = account, locale \\ "en") do
    Gettext.put_locale(locale)
    instance_url = get_config(:instance)
    base_email()
    |> to(account.email)
    |> subject(gettext "Peakweaver: Reset your password on %{instance} instructions", instance: instance_url)
    |> put_header("Reply-To", get_config(:reply_to))
    |> assign(:token, account.reset_password_token)
    |> assign(:instance, instance_url)
    |> render(:password_reset)
  end

  defp base_email do
    # Here you can set a default from, default headers, etc.
    new_email()
    |> from(Application.get_env(:pickweaver, PickweaverWeb.Endpoint)[:email_from])
    |> put_html_layout({Pickweaver.EmailView, "email.html"})
    |> put_text_layout({Pickweaver.EmailView, "email.text"})
  end

  @spec get_config(atom()) :: any()
  defp get_config(key) do
    config = Application.get_env(:pickweaver, PickweaverWeb.Endpoint) |> Keyword.get(key)
  end
end
