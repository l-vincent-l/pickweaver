defmodule Pickweaver.Mixfile do
  use Mix.Project

  def project do
    [
      app: :pickweaver,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext, :phoenix_swagger] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: ["coveralls": :test, "coveralls.detail": :test, "coveralls.post": :test, "coveralls.html": :test],
      name: "Pickweaver",
      source_url: "https://framagit.org/framasoft/pickweaver",
      homepage_url: "https://framagit.org/framasoft/pickweaver",
      docs: [main: "Pickweaver"]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Pickweaver.Application, []},
      extra_applications: [:logger, :runtime_tools, :timex],
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.0"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.2"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
      {:cors_plug, "~> 1.2"},
      {:extwitter, "~> 0.8"},
      {:earmark, "~> 1.2"},
      {:html_sanitize_ex, "~> 1.3.0-rc3"},
      {:html_entities, "~> 0.4"},
      {:open_graph_extended, "~> 0.1.1"},
      {:guardian, "~> 1.0"},
      {:guardian_db, "~> 1.0"},
      {:comeonin, "~> 4.0"},
      {:argon2_elixir, "~> 1.2"},
      {:timex, "~> 3.1"},
      {:meeseeks, "~> 0.7.6"},
      {:fuzzyurl, "~> 0.9.0"},
      {:httpoison, "~> 0.13"},
      {:poison, "~> 3.1"},
      {:ecto_autoslug_field, "~> 0.3"},
      {:logger_file_backend, "~> 0.0.10"},
      {:exgravatar, "~> 2.0.1"},
      {:extus, "~> 0.1.0"},
      {:ecto_enum, "~> 1.0"},
      {:cachex, "~> 3.0"},
      {:phoenix_swagger, "~> 0.8"},
      {:ex_json_schema, "~> 0.5"},
      {:simplehttp, "~> 0.4.1"},
      {:bamboo, "~> 0.8"},
      {:bamboo_smtp, "~> 1.4.0"},
      {:distillery, "~> 1.5", runtime: false},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false},
      {:credo, "~> 0.9.0-rc1", only: [:dev, :test], runtime: false},
      {:ex_machina, "~> 2.1", only: :test},
      {:scrivener_ecto, "~> 1.0"},
      {:scrivener_headers, "~> 3.1"},
      {:excoveralls, "~> 0.8", only: :test},
      {:exvcr, "~> 0.10", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test": ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
